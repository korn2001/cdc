<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">

<style>
	body {
		font-family: 'Ubuntu', sans-serif !important;
	}
</style>
<nav class="navbar navbar-expand navbar-light navbar-bg" style="height:50px!important;">
	<a id="navbutton" class="sidebar-toggle js-sidebar-toggle ml-3">
		<i class="hamburger align-self-center"></i>
	</a>
	<div class="navbar-collapse collapse">
		<ul class="navbar-nav navbar-align">
			<li class="nav-item dropdown mr-3d">
				<a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
					<i class="align-middle" data-feather="settings"></i>
				</a>
				<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
					<img src="img/avatars/avatar.jpg" class="avatar img-fluid rounded me-1" style="width:25px; height:25px;" /> <span class="text-dark" style="font-size:12px;"><?php echo $_SESSION["fullname"]; ?></span>
				</a>
				<div class="dropdown-menu dropdown-menu-end">
					<a class="dropdown-item" style="font-size:12px;" href="./setting.php">Setting</a>
					<a class="dropdown-item" style="font-size:12px;" type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Action</a>
					<a class="dropdown-item" style="font-size:12px;" href="./api/api-logout.php">Logout</a>
				</div>
			</li>
		</ul>
	</div>
</nav>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<table class="table mt-3">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Username</th>
						<th scope="col">Action</th>
						<th scope="col">Timestamp</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$companyid = $_SESSION["idcompany"];
					include("./api/config.php");
					$sql = "SELECT * FROM tbl_log WHERE id_company= '$companyid' ORDER BY id DESC ";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) {
					?>
							<tr>
								<td><?php echo $row["id"] ?></td>
								<td><?php echo $row["username"] ?></td>
								<td><?php echo $row["action"] ?></td>
								<td><?php echo $row["time"] ?></td>
							</tr>
					<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
	$(document).ready(function() {

		$('#navbutton').click(function(e) {
			e.preventDefault();
			$('#sidebar').toggle('slow');
		});

	});
</script>