<?php
// session_start();
include 'var.php';

$sum_critical = 0;
$sum_high = 0;
$sum_medium = 0;
$sum_low = 0;

$ratchaburi_low = 0;
$ratchaburi_medium = 0;
$ratchaburi_high = 0;
$ratchaburi_critical = 0;

$chonburi_low = 0;
$chonburi_medium = 0;
$chonburi_high = 0;
$chonburi_critical = 0;

$neurologicalinstitute_low = 0;
$neurologicalinstitute_medium = 0;
$neurologicalinstitute_high = 0;
$neurologicalinstitute_critical = 0;

$maharatnakhon_low = 0;
$maharatnakhon_medium = 0;
$maharatnakhon_high = 0;
$maharatnakhon_critical = 0;

$hatyai_low = 0;
$hatyai_medium = 0;
$hatyai_high = 0;
$hatyai_critical = 0;

foreach ($_SESSION["parsed_json"] as $key => $value) {
    $parsed_json_a = json_encode($value['CustomFields'], true);
    $parsed_json_b = json_decode($parsed_json_a, true);

    if ($value['severity'] == 1) {
        $sum_low++;
    } else if ($value['severity'] == 2) {
        $sum_medium++;
    } else if ($value['severity'] == 3) {
        $sum_high++;
    } else if ($value['severity'] == 4) {
        $sum_critical++;
    }

    if ($parsed_json_b['hospitalname'] == "Hat Yai") {
        if ($value['severity'] == 1) {
            $hatyai_low++;
        } else if ($value['severity'] == 2) {
            $hatyai_medium++;
        } else if ($value['severity'] == 3) {
            $hatyai_high++;
        } else if ($value['severity'] == 4) {
            $hatyai_critical++;
        }
    } else if ($parsed_json_b['hospitalname'] == "Maharaj Nakhon") {
        if ($value['severity'] == 1) {
            $maharatnakhon_low++;
        } else if ($value['severity'] == 2) {
            $maharatnakhon_medium++;
        } else if ($value['severity'] == 3) {
            $maharatnakhon_high++;
        } else if ($value['severity'] == 4) {
            $maharatnakhon_critical++;
        }
    } else if ($parsed_json_b['hospitalname'] == "Neurological Institute") {
        if ($value['severity'] == 1) {
            $neurologicalinstitute_low++;
        } else if ($value['severity'] == 2) {
            $neurologicalinstitute_medium++;
        } else if ($value['severity'] == 3) {
            $neurologicalinstitute_high++;
        } else if ($value['severity'] == 4) {
            $neurologicalinstitute_critical++;
        }
    } else if ($parsed_json_b['hospitalname'] == "Ratchaburi") {
        if ($value['severity'] == 1) {
            $ratchaburi_low++;
        } else if ($value['severity'] == 2) {
            $ratchaburi_medium++;
        } else if ($value['severity'] == 3) {
            $ratchaburi_high++;
        } else if ($value['severity'] == 4) {
            $ratchaburi_critical++;
        }
    } else if ($parsed_json_b['hospitalname'] == "Chonburi") {
        if ($value['severity'] == 1) {
            $chonburi_low++;
        } else if ($value['severity'] == 2) {
            $chonburi_medium++;
        } else if ($value['severity'] == 3) {
            $chonburi_high++;
        } else if ($value['severity'] == 4) {
            $chonburi_critical++;
        }
    }
}

$ratchaburi_all = $ratchaburi_low + $ratchaburi_medium + $ratchaburi_high + $ratchaburi_critical;
$chonburi_all = $chonburi_low + $chonburi_medium + $chonburi_high + $chonburi_critical;
$neurologicalinstitute_all = $neurologicalinstitute_low + $neurologicalinstitute_medium + $neurologicalinstitute_high + $neurologicalinstitute_critical;
$maharatnakhon_all = $maharatnakhon_low + $maharatnakhon_medium + $maharatnakhon_high + $maharatnakhon_critical;
$hatyai_all = $hatyai_low + $hatyai_medium + $hatyai_high + $hatyai_critical;
