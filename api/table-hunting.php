<?php
$count = 0;
foreach ($_SESSION["parsed_json"] as $key => $value) {
    if ($value['type'] == "MDR - CVE Hunting" || $value['type'] == "MDR - Threat IOC Hunting") {
?>
        <tr>
            <!-- id -->
            <td><?php echo $value['id'] ?></td>
            <!-- ---------------------------------------------------- -->
            <!-- rawName -->
            <td><?php echo $value['rawName'] ?></td>
            <!-- ---------------------------------------------------- -->
            <!-- Severity -->
            <td><?php echo $value['type'] ?></td>
            <!-- ---------------------------------------------------- -->

            <?php
            if ($value['status'] == 0) {
                echo '<td class="hidden_mobile"><span class="badge bg-warning">Pending</span></td>';
            } else if ($value['status'] == 1) {
                echo '<td class="hidden_mobile"><span class="badge bg-primary">Open</span></td>';
            } else if ($value['status'] == 2) {
                echo '<td class="hidden_mobile"><span class="badge bg-success">Closed</span></td>';
            }

            $parsed_json_a = json_encode($value['CustomFields'], true);
            $parsed_json_b = json_decode($parsed_json_a, true);

            if (isset($parsed_json_b["mdrcveaffectedendpoint"])) {
                if ($parsed_json_b["mdrcveaffectedendpoint"] == "Yes") {
                    echo '<td><span class="badge bg-warning">Found</span></td>';
                } else if ($parsed_json_b["mdrcveaffectedendpoint"] == "No") {
                    echo '<td><span class="badge bg-info">Not Found</span></td>';
                } else {
                    echo '<td><span class="badge bg-info">Not Found</span></td>';
                }
            } else {
                echo '<td><span class="badge bg-secondary">Not Found</span></td>';
            }
            ?>
            <td class="d-none d-xl-table-cell hidden_mobile">
                <?php
                $datearr = explode(".", $value['occurred']);
                $date = $datearr[0];
                $date = date_format(date_create($date), 'Y-m-d h:i:s');
                $date = date('Y-m-d h:i:s', strtotime($date . ' + 7 hours'));
                echo date('d-M-Y h:i a', strtotime($date));
                ?></td>
            <?php

            if ($value['status'] == 0 || $value['status'] == 1) {
                echo '<td class="hidden_mobile d-none d-xl-table-cell">-</td>';
            } else {
                $datearrclosed = explode(".", $value['closed']);
                $dateclosed = $datearrclosed[0];
                $dateclosed = date_format(date_create($dateclosed), 'Y-m-d h:i:s');
                $dateclosed = date('Y-m-d h:i:s', strtotime($dateclosed . ' + 7 hours'));
                $dateclosed = date('d-M-Y h:i a', strtotime($dateclosed));
                echo '<td class="hidden_mobile d-none d-xl-table-cell">' . $dateclosed . '</td>';
            }
            ?>
        </tr>
<?php
    }
}
