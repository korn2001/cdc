<?php
foreach ($_SESSION["endpoint"] as $key => $value) {
?>
    <tr>
        <td class="hidden_mobile"><?php echo $key + 1; ?></td>
        <td style="font-size:12px;"><i class="align-middle" data-feather="monitor"></i> &nbsp; <?php echo $value['endpoint_name']; ?></td>
        <td class="hidden_mobile"><?php echo $value['endpoint_status']; ?></td>
        <td>
            <?php
            if ($value['is_isolated'] == "AGENT_UNISOLATED") {
                echo '<a onclick="isolate(\'api/isolate.php?id=' . $value['endpoint_id'] . '\' , \'' . $value['endpoint_name'] . '\')"><img src="./img/icons/on.png" alt="" style="width:40px;"></a></br>';
                echo '<small>' . str_replace("agent_", "", strtolower($value['is_isolated'])) . '</small>';
            } else if ($value['is_isolated'] == "AGENT_ISOLATED") {
                echo '<a onclick="unisolate(\'api/unisolate.php?id=' . $value['endpoint_id'] . '\' , \'' . $value['endpoint_name'] . '\')"><img src="./img/icons/off.png" alt="" style="width:40px;"></a></br>';
                echo '<small>' . str_replace("agent_", "", strtolower($value['is_isolated'])) . '</small>';
            } else if ($value['is_isolated'] == "AGENT_PENDING_ISOLATION") {
                echo '<a onclick="unisolate(\'api/unisolate.php?id=' . $value['endpoint_id'] . '\' , \'' . $value['endpoint_name'] . '\')"><img src="./img/icons/wait.png" alt="" style="width:40px;"></a></br>';
                echo '<small>' . str_replace("agent_", "", strtolower($value['is_isolated'])) . '</small>';
            } else {
                echo '<a onclick="unisolate(\'api/unisolate.php?id=' . $value['endpoint_id'] . '\' , \'' . $value['endpoint_name'] . '\')"><img src="./img/icons/wait.png" alt="" style="width:40px;"></a></br>';
                echo '<small>' . "pending" . '</small>';
            }
            ?> 
        </td>
        <td onclick="scan( 'url' , 'ADMINMDR-LABTEST')" class="hidden_mobile"><i class="align-middle" data-feather="search" style="color: orange;"></i> &nbsp;&nbsp;<?php echo $value['scan_status'] ?></td>
    </tr>
<?php } ?>

<script>
    function isolate(URL, HOST) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'Want to isolate ' + HOST,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, isolate it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    'Isolate!',
                )
                window.location.href = URL;
            }
        })
    }

    function unisolate(URL, HOST) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'Want to unisolate ' + HOST,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, unisolate it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    'Unisolate!',
                )
                window.location.href = URL;
            }
        })
    }

    function scan(URL="url", HOST) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'Want to scan ' + HOST,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, scan it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    'Scan!',
                )
                // window.location.href = URL;
            }
        })
    }
</script>