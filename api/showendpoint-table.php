<?php
foreach ($_SESSION["endpoint"] as $key => $value) {
?>
    <tr>
        <td class="hidden_mobile"><?php echo $key + 1; ?></td>
        <td style="font-size:12px;"><i class="align-middle" data-feather="monitor"></i> &nbsp; <?php echo $value['endpoint_name']; ?></td>
        <td class="hidden_mobile"><?php echo str_replace("agent_type_", "", strtolower($value['endpoint_type'])); ?></td>
        <td class="hidden_mobile"><?php echo $value['operating_system']; ?></td>
        <?php
        if ($value['endpoint_status'] == "DISCONNECTED") {
            echo "<td class=\"hidden_mobile\" style=\"text-align: center;\">
        <i class=\"align-middle\" data-feather=\"monitor\" style=\"color: red;\"></i> <br> 
        " . $value['endpoint_status'] . "
      </td>";
        } else if ($value['endpoint_status'] == "CONNECTED") {
            echo "<td class=\"hidden_mobile\" style=\"text-align: center;\">
        <i class=\"align-middle\" data-feather=\"monitor\" style=\"color: green;\"></i> <br> 
        " . $value['endpoint_status'] . "
      </td>";
        } else if ($value['endpoint_status'] == "LOST") {
            echo "<td class=\"hidden_mobile\" style=\"text-align: center;\">
        <i class=\"align-middle\" data-feather=\"monitor\" style=\"color: black;\"></i> <br> 
        " . $value['endpoint_status'] . "
      </td>";
        } else {
            echo "<td class=\"hidden_mobile\" style=\"text-align: center;\">
        <i class=\"align-middle\" data-feather=\"monitor\" style=\"color: black;\"></i> <br> 
        " . $value['endpoint_status'] . "
      </td>";
        }
        ?>

        <td class="hidden_mobile"><?php echo date("d-M-Y H:i:s", substr($value['last_seen'], 0, 10)); ?></td>
    </tr>
<?php } ?>