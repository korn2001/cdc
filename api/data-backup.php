<?php
session_start();
$company_url = $_SESSION["link"];
$keyapi = $_SESSION["key"];
$day_data = $_SESSION["day"];
$data = '
{
    "filter": {
      "fields" : [ "id","rawName","closed","occurred","status","closed","xdralerts" ],
      "query" : "hospitalname:\"Hat Yai\"",
      "page": 0,
      "sort": [
        {
          "desc": true,
          "field": "id"
        }
      ],
      "period": {
        "by": "day",
        "fromValue": ' . $day_data . '
      }
    }
}
';
$url = $company_url;
$method = "POST";
$curl = curl_init();
switch ($method) {
  case "POST":
    curl_setopt($curl, CURLOPT_POST, 1);
    if ($data)
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    break;
  case "PUT":
    curl_setopt($curl, CURLOPT_PUT, 1);
    break;
  default:
    if ($data)
      $url = sprintf("%s?%s", $url, $data);
}
$headers = array(
  "Content-Type: application/json",
  "Authorization: " . strval($keyapi)
);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_ENCODING, "");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
$result = curl_exec($curl);
curl_close($curl);

$parsed_json = json_decode($result, true);
$page = ceil($parsed_json['total'] / 25) - 1;
$_SESSION["total"] = $parsed_json['total'];
$parsed_json_all = $parsed_json['data'];

for ($i = 1; $i <= $page; $i++) {
  $data = '
    {
        "filter": {
          "fields" : [ "id","rawName","closed","occurred","status","closed","xdralerts" ],
          "query" : "hospitalname:\"Hat Yai\"",
          "page": ' . $i . ',
          "sort": [
            {
              "desc": true,
              "field": "id"
            }
          ],
          "period": {
            "by": "day",
            "fromValue": ' . $day_data . '
          }
        }
    }
    ';
  $url = $company_url;;
  $method = "POST";
  $curl = curl_init();
  switch ($method) {
    case "POST":
      curl_setopt($curl, CURLOPT_POST, 1);
      if ($data)
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      break;
    case "PUT":
      curl_setopt($curl, CURLOPT_PUT, 1);
      break;
    default:
      if ($data)
        $url = sprintf("%s?%s", $url, $data);
  }
  $headers = array(
    "Content-Type: application/json",
    "Authorization: " . strval($keyapi)
  );
  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_ENCODING, "");
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
  $result = curl_exec($curl);
  curl_close($curl);
  $parsed_json = json_decode($result, true);
  $parsed_json_all = array_merge($parsed_json_all, $parsed_json['data']);
}

var_dump($parsed_json_all);
$_SESSION["parsed_json"] = $parsed_json_all;

?>


