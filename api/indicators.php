<?php
$company_url = "https://02019522732.demisto.works/indicators/search";
$keyapi = $_SESSION["key"];
$data = '
{
    "filter":{
       "query":"",
       "sort":[
          {
             "desc":true,
             "field":"id"
          }
       ]  
    }
 }';

$url = $company_url;
$method = "POST";
$curl = curl_init();
switch ($method) {
    case "POST":
        curl_setopt($curl, CURLOPT_POST, 1);
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
    case "PUT":
        curl_setopt($curl, CURLOPT_PUT, 1);
        break;
    default:
        if ($data)
            $url = sprintf("%s?%s", $url, $data);
}
$headers = array(
    "Content-Type: application/json",
    "Authorization: " . strval($keyapi)
);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_ENCODING, "");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
$result = curl_exec($curl);
curl_close($curl);

$parsed_json = json_decode($result, true);
$_SESSION["ti"] = $parsed_json['iocObjects'];
