<?php
session_start();
$xdr_id = $_SESSION["xdrid"];
$xdr_key = $_SESSION["xdrkey"];
$xdr_url = $_SESSION["xdrurl"];

// echo $xdr_id;
// echo $xdr_key; 
// echo $xdr_url;
$parsed_json_all = array();
$data =
    '{
    "request_data": {
        "search_from": 0,
        "sort": {
            "field": "endpoint_id",
            "keyword": "DESC"
        },
        "search_to": 1
    }
}';

$headers = array(
    "Content-Type: application/json",
    "Authorization: " . $xdr_key,
    "x-xdr-auth-id: " . $xdr_id
);

$url = $xdr_url."/public_api/v1/endpoints/get_endpoint/";
$method = "POST";
$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_ENCODING, "");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
$result = curl_exec($curl);
curl_close($curl);
$parsed_json = json_decode($result, true);
$parsed_json = $parsed_json["reply"];
$page = ceil($parsed_json['total_count'] / 100);

$range = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];

for ($i = 0; $i < $page; $i++) {
    $data =
        '{
        "request_data": {
            "search_from": ' . $range[$i] . ',
            "sort": {
                "field": "endpoint_id",
                "keyword": "DESC"
            },
            "search_to": ' . $range[$i + 1] . '
        }
    }';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_ENCODING, "");
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    $result = curl_exec($curl);
    curl_close($curl);

    $parsed_json = json_decode($result, true);
    $parsed_json = $parsed_json["reply"];
    $parsed_json_all = array_merge($parsed_json_all, $parsed_json["endpoints"]);
}

$_SESSION["endpoint"] = $parsed_json_all;
