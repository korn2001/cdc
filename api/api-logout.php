<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<?php 
   session_start(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Logout</title>
</head>
<body>
<?php 
    $_SESSION["id"] = "";
    $_SESSION["username"]= "";
    $_SESSION["password"]= "";
    $_SESSION["fullname"] = "";
    $_SESSION["tel"] = "";
    $_SESSION["email"] = "";
    $_SESSION["role"] = "";
    $_SESSION["company"] = "";
    $_SESSION["link"] = "";
    $_SESSION["key"] = "";
    $_SESSION["status"] = "";
    echo'<script>swal("Logout Success", "" , "success");</script>';
    sleep(1);
    echo '<script>  document.location.href = "../index.php";</script> '; 
?>

</body>
</html>