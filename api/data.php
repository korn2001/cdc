<?php
// session_start();
$url = $_SESSION["link"];
$keyapi = $_SESSION["key"];
$day_data = $_SESSION["day"];
$key_query = $_SESSION["key_query"];
$parsed_json_all = array();
$data = '
{
    "filter": {
      "fields" : ["id"],
      "query" : "' . $key_query . '",
      "page": 0,
      "sort": [
        {
          "desc": true,
          "field": "id"
        }
      ],
      "period": {
        "by": "day",
        "fromValue": ' . $day_data . '
      }
    }
}
';

$headers = array(
  "Content-Type: application/json",
  "Authorization: " . strval($keyapi)
);
$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_ENCODING, "");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
$result = curl_exec($curl);
curl_close($curl);

$parsed_json = json_decode($result, true);
$page = ceil($parsed_json['total'] / 25);

// echo $parsed_json['total'];
// echo "-";
// echo $page;

$curl_arr = array();
$master = curl_multi_init();
// $pageofpage = ceil($page / 200);

for ($i = 0; $i < $page; $i++) {
  $data_page = '
{
    "filter": {
      "fields" : [ "id","rawName","closed","occurred","status","closed","xdralerts" ],
      "query" : "' . $key_query . '",
      "page": ' . $i . ',
      "sort": [
        {
          "desc": true,
          "field": "id"
        }
      ],
      "period": {
        "by": "day",
        "fromValue": ' . $day_data . '
      }
    }
}
';
  $curl_arr[$i] = curl_init($url);
  curl_setopt($curl_arr[$i], CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl_arr[$i], CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl_arr[$i], CURLOPT_ENCODING, "");
  curl_setopt($curl_arr[$i], CURLOPT_URL, $url);
  curl_setopt($curl_arr[$i], CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl_arr[$i], CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($curl_arr[$i], CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl_arr[$i], CURLOPT_POST, true);
  curl_setopt($curl_arr[$i], CURLOPT_POSTFIELDS, $data_page);
  curl_multi_add_handle($master, $curl_arr[$i]);
}

do {
  $status = curl_multi_exec($master, $running);
  if ($running) {
    curl_multi_select($master);
  }
} while ($running > 0);

for ($i = 0; $i < $page; $i++) {
  $results = curl_multi_getcontent($curl_arr[$i]);
  $parsed_json = json_decode($results, true);
  if (isset($parsed_json['data'])) {
    $parsed_json_all = array_merge($parsed_json_all, $parsed_json['data']);
  } else {
    // var_dump($parsed_json);
  }
}
curl_multi_close($master);
$_SESSION["parsed_json"] = $parsed_json_all;
// var_dump($parsed_json_all);
// echo sizeof($parsed_json_all);
