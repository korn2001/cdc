<?php
if (session_status() === PHP_SESSION_NONE) {
    session_start();
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', 0);
}
$xdr_id = $_SESSION["xdrid"];
$xdr_key = $_SESSION["xdrkey"];
$xdr_url = $_SESSION["xdrurl"];
$company_url = $xdr_url."/public_api/v1/endpoints/scan/";
$data =
'{
    "request_data": {
        "filters": [
            {
                "field": "endpoint_id_list",
                "operator": "in",
                "value": [
                    "'.$_GET['id'].'"
                ]
            }
        ]
    }
}';
$url = $company_url;
$method = "POST";
$curl = curl_init();
switch ($method) {
    case "POST":
        curl_setopt($curl, CURLOPT_POST, 1);
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
    case "PUT":
        curl_setopt($curl, CURLOPT_PUT, 1);
        break;
    default:
        if ($data)
            $url = sprintf("%s?%s", $url, $data);
}

$headers = array(
    "Content-Type: application/json",
    "Authorization: ".$xdr_key,
    "x-xdr-auth-id: ".$xdr_id
);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_ENCODING, "");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
$result = curl_exec($curl);
curl_close($curl);

session_start();

include("config.php");
$log_host = gethostname();
date_default_timezone_set('Asia/Bangkok');
$log_time = date('Y-m-d H:i:s');
$log_user = $_SESSION["username"];
$log_company = $_SESSION["idcompany"];
$sql_log = "INSERT INTO tbl_log ( hostname , username , id_company , time , action)
         VALUES ('$log_host','$log_user','$log_company','$log_time' ,'Isolate')";

if ($conn->query($sql_log) === TRUE) {
} else {

}

header("Location:../endpoint.php");

?>