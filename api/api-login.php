<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<body>
   <?php
   include("config.php");
   if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $compeny_id = "";
      $username = $_POST['username'];
      $password = $_POST['password'];
      $sql = "SELECT * FROM tbl_user INNER JOIN tbl_company ON tbl_user.company = tbl_company.id_company WHERE username = '$username' AND password = '$password'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
         while ($row = $result->fetch_assoc()) {
            $usedid = $row["id"];
            $_SESSION["day"] = "1";
            $_SESSION["id"] = $row["id"];
            $_SESSION["username"] = $row["username"];
            $_SESSION["password"] = $row["password"];
            $_SESSION["fullname"] = $row["fullname"];
            $_SESSION["tel"] = $row["tel"];
            $_SESSION["email"] = $row["email"];
            $_SESSION["role"] = $row["role"];
            $_SESSION["status"] = $row["status"];
            $_SESSION["company"] = $row["name_company"];
            $_SESSION["link"] = $row["link_api"];
            $_SESSION["key"] = $row["key_api"];
            $_SESSION["idcompany"] = $row["company"];

            $_SESSION["xdrid"] = $row["xdr_id"];
            $_SESSION["xdrkey"] = $row["xdr_key"];
            $_SESSION["xdrurl"] = $row["xdr_url"];

            $_SESSION["hunting"] = $row["hunting"];
            $_SESSION["ticket"] = $row["ticket"];
            $_SESSION["key_query"] = $row["key_query"];

            if ($_SESSION["role"] == "user") {
               echo '<script>  document.location.href = "../home.php";</script> ';
            } elseif ($_SESSION["role"] == "admin") {
               echo '<script>  document.location.href = "../admin/";</script> ';
            } elseif ($_SESSION["role"] == "partner") {
               echo '<script>  document.location.href = "../dashboard.php";</script> ';
            }
            ob_end_flush();
         }

         $log_host = gethostname();
         date_default_timezone_set('Asia/Bangkok');
         $log_time = date('Y-m-d H:i:s');
         $log_user = $_SESSION["username"];
         $log_company = $_SESSION["idcompany"];
         $sql_log = "INSERT INTO tbl_log ( hostname , username , id_company , time , action)
         VALUES ('$log_host','$log_user','$log_company','$log_time' ,'Login')";
         if ($conn->query($sql_log) === TRUE) {
         } else {
         }
         require("data.php");
      } else {
         echo '<script>swal("Login False!", "Please check your Email and password and try again" , "error");</script>';
         if (ob_get_level() == 2) ob_start();
         for ($i = 0; $i < 2; $i++) {
            echo str_pad('', 4096) . "\n";
            ob_flush();
            flush();
            sleep(1);
         }
         echo '<script>  document.location.href = "../index.php";</script>';
         ob_end_flush();
      }
      include("close.php");
   }
   ?>

</body>

</html>