<?php
// session_start();
include 'var.php';
foreach ($_SESSION["parsed_json"] as $key => $value) {
    array_push($sourceBrand, $value['sourceBrand']);
    $parsed_json_a = json_encode($value['CustomFields'], true);
    $parsed_json_b = json_decode($parsed_json_a, true);

    if (isset($parsed_json_b["hostnames"])) {
        foreach ($parsed_json_b["hostnames"] as $key => $val) {
            array_push($countUser, $val);
        }
    } else if (isset($parsed_json_b["mdrhostname"])) {
        array_push($countUser, $parsed_json_b["mdrhostname"]);
    }

    if (isset($parsed_json_b["xdralertname"])) {
        foreach ($parsed_json_b["xdralertname"] as $key => $val) {
            if ($key == 0) {
                array_push($countplaybook, $val);
            }
        }
    } else if (isset($value["type"])) {
        array_push($countplaybook, $value["type"]);
    }

    if ($value['severity'] == 0) {
        $unknown++;
    } else if ($value['severity'] == 0.5) {
        $infomational++;
    } else if ($value['severity'] == 1) {
        $low++;
        if (isset($parsed_json_b["mdrincidentaction"])) {
            if ($parsed_json_b["mdrincidentaction"] == 'Prevented') {
                $low_blocked++;
                array_push($low_blocked_data, $value);
            } else if ($parsed_json_b["mdrincidentaction"] == 'Detected') {
                $low_detected++;
                array_push($low_detected_data, $value);
            }
        } else if (isset($parsed_json_b["xdralerts"])) {
            foreach ($parsed_json_b["xdralerts"] as $key => $val) {
                if ($key == 0) {
                    foreach ($val as $key => $val) {
                        if ($key == "action") {
                            if ( strpos($val, 'REPORTED') !== false ) {
                                $low_detected++;
                                array_push($low_detected_data, $value);
                            }
                            if ( strpos($val, 'SCANNED') !== false ) {
                                $low_detected++;
                                array_push($low_detected_data, $value);
                            }
                            if ( strpos($val, 'DETECTED') !== false ) {
                                $low_detected++;
                                array_push($low_detected_data, $value);
                            }
                            if (  strpos($val, 'BLOCKED') !== false ) {
                                $low_blocked++;
                                array_push($low_blocked_data, $value);
                            }
                        }
                    }
                }
            }
        } else {
            if (strstr(json_encode($value["labels"]), 'REPORTED')) {
                $low_detected++;
                array_push($low_detected_data, $value);
            }
            if (strstr(json_encode($value["labels"]), 'SCANNED')) {
                $low_detected++;
                array_push($low_detected_data, $value);
            }
            if (strstr(json_encode($value["labels"]), 'DETECTED')) {
                $low_detected++;
                array_push($low_detected_data, $value);
            }
            if (strstr(json_encode($value["labels"]), 'BLOCKED')) {
                $low_blocked++;
                array_push($low_blocked_data, $value);
            }
        }
    } else if ($value['severity'] == 2) {
        $medium++;
        if (isset($parsed_json_b["mdrincidentaction"])) {
            if ($parsed_json_b["mdrincidentaction"] == 'Prevented') {
                $medium_blocked++;
                array_push($medium_blocked_data, $value);
            } else if ($parsed_json_b["mdrincidentaction"] == 'Detected') {
                $medium_detected++;
                array_push($medium_detected_data, $value);
            }
        } else if (isset($parsed_json_b["xdralerts"])) {
            foreach ($parsed_json_b["xdralerts"] as $key => $val) {
                if ($key == 0) {
                    foreach ($val as $key => $val) {
                        if ($key == "action") {
                            if ( strpos($val, 'REPORTED') !== false ) {
                                $medium_detected++;
                                array_push($medium_detected_data, $value);
                            }
                            if ( strpos($val, 'SCANNED') !== false ) {
                                $medium_detected++;
                                array_push($medium_detected_data, $value);
                            }
                            if ( strpos($val, 'DETECTED') !== false ) {
                                $medium_detected++;
                                array_push($medium_detected_data, $value);
                            }
                            if ( strpos($val, 'BLOCKED') !== false ) {
                                $medium_blocked++;
                                array_push($medium_blocked_data, $value);
                            }
                        }
                    }
                }
            }
        } else {
            if (strstr(json_encode($value["labels"]), 'REPORTED')) {
                $medium_detected++;
                array_push($medium_detected_data, $value);
            } else if (strstr(json_encode($value["labels"]), 'SCANNED')) {
                $medium_detected++;
                array_push($medium_detected_data, $value);
            } else if (strstr(json_encode($value["labels"]), 'DETECTED')) {
                $medium_detected++;
                array_push($medium_detected_data, $value);
            } else if (strstr(json_encode($value["labels"]), 'BLOCKED')) {
                $medium_blocked++;
                array_push($medium_blocked_data, $value);
            }
        }
    } else if ($value['severity'] == 3) {
        $high++;
        if (isset($parsed_json_b["mdrincidentaction"])) {
            if ($parsed_json_b["mdrincidentaction"] == 'Prevented') {
                $high_blocked++;
                array_push($high_blocked_data, $value);
            } else if ($parsed_json_b["mdrincidentaction"] == 'Detected') {
                $high_detected++;
                array_push($high_detected_data, $value);
            }
        } else if (isset($parsed_json_b["xdralerts"])) {
            foreach ($parsed_json_b["xdralerts"] as $key => $val) {
                if ($key == 0) {
                    foreach ($val as $key => $val) {
                        if ($key == "action") {
                            if ( strpos($val, 'REPORTED') !== false ) {
                                $high_detected++;
                                array_push($high_detected_data, $value);
                            }
                            if ( strpos($val, 'SCANNED') !== false ) {
                                $high_detected++;
                                array_push($high_detected_data, $value);
                            }
                            if ( strpos($val, 'DETECTED') !== false ) {
                                $high_detected++;
                                array_push($high_detected_data, $value);
                            }
                            if ( strpos($val, 'BLOCKED') !== false ) {
                                $high_blocked++;
                                array_push($high_blocked_data, $value);
                            }
                        }
                    }
                }
            }
        } else {
            if (strstr(json_encode($value["labels"]), 'REPORTED')) {
                $high_detected++;
                array_push($high_detected_data, $value);
            } else if (strstr(json_encode($value["labels"]), 'SCANNED')) {
                $high_detected++;
                array_push($high_detected_data, $value);
            } else if (strstr(json_encode($value["labels"]), 'DETECTED')) {
                $high_detected++;
                array_push($high_detected_data, $value);
            } else if (strstr(json_encode($value["labels"]), 'BLOCKED')) {
                $high_blocked++;
                array_push($high_blocked_data, $value);
            }
        }
    } else if ($value['severity'] == 4) {
        $critical++;
        if (isset($parsed_json_b["xdralerts"])) {

            if (isset($parsed_json_b["mdrincidentaction"])) {
                if ($parsed_json_b["mdrincidentaction"] == 'Prevented') {
                    $critical_blocked++;
                    array_push($critical_blocked_data, $value);
                } else if ($parsed_json_b["mdrincidentaction"] == 'Detected') {
                    $critical_detected++;
                    array_push($critical_detected_data, $value);
                }
            } else if (isset($parsed_json_b["xdralerts"])) {
                foreach ($parsed_json_b["xdralerts"] as $key => $val) {
                    if ($key == 0) {
                        foreach ($val as $key => $val) {
                            if ($key == "action") {
                                if ( strpos($val, 'REPORTED') !== false ) {
                                    $critical_detected++;
                                    array_push($critical_detected_data, $value);
                                }
                                if ( strpos($val, 'SCANNED') !== false ) {
                                    $critical_detected++;
                                    array_push($critical_detected_data, $value);
                                }
                                if ( strpos($val, 'DETECTED') !== false ) {
                                    $critical_detected++;
                                    array_push($critical_detected_data, $value);
                                }
                                if ( strpos($val, 'BLOCKED') !== false ) {
                                    $critical_blocked++;
                                    array_push($critical_blocked_data, $value);
                                }
                            }
                        }
                    }
                }
            } else {
                if (strstr(json_encode($value["labels"]), 'REPORTED')) {
                    $critical_detected++;
                    array_push($critical_detected_data, $value);
                } else if (strstr(json_encode($value["labels"]), 'SCANNED')) {
                    $critical_detected++;
                    array_push($critical_detected_data, $value);
                } else if (strstr(json_encode($value["labels"]), 'DETECTED')) {
                    $critical_detected++;
                    array_push($critical_detected_data, $value);
                } else if (strstr(json_encode($value["labels"]), 'BLOCKED')) {
                    $critical_blocked++;
                    array_push($critical_blocked_data, $value);
                }
            }
        }
    }
    if ($value['status'] == 0) {
        $pending++;
    } else if ($value['status'] == 1) {
        $open++;
    } else if ($value['status'] == 2) {
        $closed++;
    }
    $datearr = explode(".", $value['occurred']);
    $date = $datearr[0];
    $date = date_format(date_create($date), 'Y-m-d h:i:s');
    $date = date('Y-m-d h:i:s', strtotime($date . ' + 7 hours'));
    $date = date('d-M-Y h:i a', strtotime($date));
}

// $countaction = array_count_values($countaction);
// arsort($countaction, SORT_NUMERIC);

$DETECTED += $low_detected;
$DETECTED += $medium_detected;
$DETECTED += $high_detected;
$DETECTED += $critical_detected;

$BLOCKED += $low_blocked;
$BLOCKED += $medium_blocked;
$BLOCKED += $high_blocked;
$BLOCKED += $critical_blocked;

// $countactionlow = array_count_values($countactionlow);
// arsort($countactionlow, SORT_NUMERIC);
// foreach ($countactionlow as $key => $val) {
//     array_push($topcountactionlow, $key . " [" . $val . "]");
// }

// $countactionmedium = array_count_values($countactionmedium);
// arsort($countactionmedium, SORT_NUMERIC);
// foreach ($countactionmedium as $key => $val) {
//     array_push($topcountactionmedium, $key . " [" . $val . "]");
// }

// $countactionhigh = array_count_values($countactionhigh);
// arsort($countactionhigh, SORT_NUMERIC);
// foreach ($countactionhigh as $key => $val) {
//     array_push($topcountactionhigh, $key . " [" . $val . "]");
// }

// $countactioncritical = array_count_values($countactioncritical);
// arsort($countactioncritical, SORT_NUMERIC);
// foreach ($countactioncritical as $key => $val) {
//     array_push($topcountactioncritical, $key . " [" . $val . "]");
// }

$countplaybook = array_count_values($countplaybook);
arsort($countplaybook, SORT_NUMERIC);
foreach ($countplaybook as $key => $val) {
    // array_push($topcountplaybook, $val . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . implode(' ', array_slice(explode(' ', $key), 0, 4)) . "...");
    array_push($topcountplaybook, $val . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $key);
}

$countUser = array_count_values($countUser);
arsort($countUser, SORT_NUMERIC);
foreach ($countUser as $key => $val) {
    // array_push($topcountUser, $val . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . implode(' ', array_slice(explode(' ', $key), 0, 10)) );
    array_push($topcountUser, $val . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $key);
}

$count_sourceBrand = array_count_values($sourceBrand);
arsort($count_sourceBrand, SORT_NUMERIC);
foreach ($count_sourceBrand as $key => $val) {
    // array_push($topsourceBrand, $val . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . implode(' ', array_slice(explode(' ', $key), 0, 5)) );
    array_push($topsourceBrand, $val . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $key);
}