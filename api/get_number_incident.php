<?php
session_start();
$url = $_SESSION["link"];
$keyapi = $_SESSION["key"];
$day_data = $_GET['day'];
$key_query = $_SESSION["key_query"];
$parsed_json_all = array();
$data = '
{
    "filter": {
      "fields" : ["id"],
      "query" : "' . $key_query . '",
      "page": 0,
      "sort": [
        {
          "desc": true,
          "field": "id"
        }
      ],
      "period": {
        "by": "day",
        "fromValue":' . $day_data . '
      }
    }
}
';

$headers = array(
  "Content-Type: application/json",
  "Authorization: " . strval($keyapi)
);
$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_ENCODING, "");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
$result = curl_exec($curl);
curl_close($curl);

$parsed_json = json_decode($result, true);
echo $parsed_json['total'];

 ?>