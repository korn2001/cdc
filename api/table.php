<?php
foreach ($_SESSION["parsed_json"] as $key => $value) {
    if ($value['type'] != "MDR - CVE Hunting" && $value['type'] != "MDR - Threat IOC Hunting" && $value['type'] != "MDR - On-Demand Incident" && $value['type'] != "MDR - Ticket Incident") {
?>
        <tr>
            <!-- id -->
            <td><?php echo $value['id'] ?></td>
            <!-- ---------------------------------------------------- -->

            <!-- rawName -->
            <td><?php echo $value['rawName'] ?></td>
            <!-- ---------------------------------------------------- -->

            <!-- Severity -->
            <?php
            if ($value['severity'] == 0) {
                echo '<td class="d-none d-md-table-cell"><img src="./img/icons/1.png" style="width:20px;" alt="">Unknown</td>';
            } else if ($value['severity'] == 0.5) {
                echo '<td class="d-none d-md-table-cell"><img src="./img/icons/2.png" style="width:20px;" alt="">Infomational</td>';
            } else if ($value['severity'] == 1) {
                echo '<td class="d-none d-md-table-cell"><img src="./img/icons/3.png" style="width:20px;" alt="">Low</td>';
            } else if ($value['severity'] == 2) {
                echo '<td class="d-none d-md-table-cell"><img src="./img/icons/4.png" style="width:20px;" alt="">Medium</td>';
            } else if ($value['severity'] == 3) {
                echo '<td class="d-none d-md-table-cell"><img src="./img/icons/5.png" style="width:20px;" alt="">High</td>';
            } else if ($value['severity'] == 4) {
                echo '<td class="d-none d-md-table-cell"><img src="./img/icons/6.png" style="width:20px;" alt="">Critical</td>';
            }
            // <!-- ---------------------------------------------------- -->


            if ($value['status'] == 0) {
                echo '<td class="hidden_mobile"><span class="badge bg-warning">Pending</span></td>';
            } else if ($value['status'] == 1) {
                echo '<td class="hidden_mobile"><span class="badge bg-primary">Open</span></td>';
            } else if ($value['status'] == 2) {
                echo '<td class="hidden_mobile"><span class="badge bg-dark">Closed</span></td>';
            }

            $parsed_json_a = json_encode($value['CustomFields'], true);
            $parsed_json_b = json_decode($parsed_json_a, true);

            // ----------------------

            if (isset($parsed_json_b["mdrincidentaction"])) {
                if ($parsed_json_b["mdrincidentaction"] == 'Prevented') {
                    echo '<td><span class="badge bg-success">Blocked</span></td>';
                } else if ($parsed_json_b["mdrincidentaction"] == 'Detected') {
                    echo '<td><span class="badge bg-warning">Detected</span></td>';
                } else {
                    echo '<td><span class="badge bg-secondary">N/A</span></td>';
                }
            } else if (isset($parsed_json_b["xdralerts"])) {
                foreach ($parsed_json_b["xdralerts"] as $key => $val) {
                    if ($key == 0) {
                        foreach ($val as $key => $val) {
                            if ($key == "action") {
                                if (strpos($val, 'REPORTED') !== false) {
                                    echo '<td><span class="badge bg-warning">Detected</span></td>';
                                } else if (strpos($val, 'SCANNED') !== false) {
                                    echo '<td><span class="badge bg-warning">Detected</span></td>';
                                } else if (strpos($val, 'DETECTED') !== false) {
                                    echo '<td><span class="badge bg-warning">Detected</span></td>';
                                } else if (strpos($val, 'BLOCKED') !== false) {
                                    echo '<td><span class="badge bg-success">Blocked</span></td>';
                                } else {
                                    echo '<td><span class="badge bg-secondary">N/A</span></td>';
                                }
                            }
                        }
                    }
                }
            } else {
                if (strstr(json_encode($value["labels"]), 'REPORTED')) {
                    echo '<td><span class="badge bg-warning">Detected</span></td>';
                } else if (strstr(json_encode($value["labels"]), 'SCANNED')) {
                    echo '<td><span class="badge bg-warning">Detected</span></td>';
                } else if (strstr(json_encode($value["labels"]), 'DETECTED')) {
                    echo '<td><span class="badge bg-warning">Detected</span></td>';
                } else if (strstr(json_encode($value["labels"]), 'BLOCKED')) {
                    echo '<td><span class="badge bg-success">Blocked</span></td>';
                } else {
                    echo '<td><span class="badge bg-secondary">N/A</span></td>';
                }
            }
            // ----------------------

            // if (isset($parsed_json_b["xdralerts"])) {
            //     foreach ($parsed_json_b["xdralerts"] as $key => $val) {
            //         if ($key == 0 && $val != null) {
            //             foreach ($val as $key => $val) {
            //                 if ($key == "action") {
            //                     if ($val == 'BLOCKED') {
            //                         echo '<td><span class="badge bg-success">Blocked</span></td>';
            //                     } else {
            //                         echo '<td><span class="badge bg-warning">Detected</span></td>';
            //                     }
            //                 }
            //             }
            //         }else if($key == 0 && $val == null){
            //             if(isset($parsed_json_b["mdrincidentaction"])){
            //                 if ($parsed_json_b["mdrincidentaction"] == 'Prevented') {
            //                     echo '<td><span class="badge bg-success">Blocked</span></td>';
            //                 } else {
            //                     echo '<td><span class="badge bg-warning">Detected</span></td>';
            //                 }
            //             }else{
            //                 echo '<td><span class="badge bg-secondary">N/A</span></td>';
            //             }
            //         }
            //     }
            // }else{
            //     if(isset($parsed_json_b["mdrincidentaction"])){
            //         if ($parsed_json_b["mdrincidentaction"] == 'Prevented') {
            //             echo '<td><span class="badge bg-success">Blocked</span></td>';
            //         } else {
            //             echo '<td><span class="badge bg-warning">Detected</span></td>';
            //         }
            //     }else{
            //         echo '<td><span class="badge bg-secondary">N/A</span></td>';
            //     }
            // }

            ?>
            <td class="d-none d-xl-table-cell hidden_mobile">
                <?php
                $datearr = explode(".", $value['occurred']);
                $date = $datearr[0];
                $date = date_format(date_create($date), 'Y-m-d h:i:s');
                $date = date('Y-m-d h:i:s', strtotime($date . ' + 7 hours'));
                echo date('d-M-Y h:i a', strtotime($date));
                ?></td>
            <?php

            if ($value['status'] == 0 || $value['status'] == 1) {
                echo '<td class="hidden_mobile d-none d-xl-table-cell">-</td>';
            } else {
                $datearrclosed = explode(".", $value['closed']);
                $dateclosed = $datearrclosed[0];
                $dateclosed = date_format(date_create($dateclosed), 'Y-m-d h:i:s');
                $dateclosed = date('Y-m-d h:i:s', strtotime($dateclosed . ' + 7 hours'));
                $dateclosed = date('d-M-Y h:i a', strtotime($dateclosed));
                echo '<td class="hidden_mobile d-none d-xl-table-cell">' . $dateclosed . '</td>';
            }
            ?>
        </tr>

<?php
    }
}
?>