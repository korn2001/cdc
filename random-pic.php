
<?php
function getRandomImage()
{
    // กำหนด path ของโฟลเดอร์ที่เก็บรูปภาพ
    $imageFolder = './img/uploads/';

    // รายชื่อรูปภาพทั้งหมดในโฟลเดอร์
    $imageFiles = scandir($imageFolder);

    // กรองรูปภาพออกจากรายชื่อที่ไม่ต้องการ (เช่น . และ ..)
    $imageFiles = array_diff($imageFiles, array('.', '..', '.DS_Store'));

    // สุ่มเลือกรูปภาพจากอาร์เรย์
    $randomImage = $imageFiles[array_rand($imageFiles)];

    // กำหนด path ของรูปภาพที่ถูกสุ่ม
    $imagePath = $imageFolder . $randomImage;

    return $imagePath;
}

?>