<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Permission Denied</title>
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="sweetalert2.min.css">

    <script>
        function checklogin() {
            Swal.fire({
                icon: 'error',
                title: 'Oops... Permission Denied',
                text: 'Please Login!',
                // footer: '<a href="/">Login?</a>'
            }).then((result) => {
                if (result.isConfirmed) {
                    document.location.href = "./index.php";
                }
            })
        }
        checklogin();
    </script>
</body>
</html>