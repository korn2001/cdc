<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<title>MDR Center</title>
	<link href="css/app.css" rel="stylesheet">
	<style>
		@media screen and (min-width: 0px) and (max-width: 767px) {
			.hide {
				display: none;
			}
		}
	</style>
</head>
<?php include "loader.php"; ?>
<?php include 'random-pic.php'; ?>

<body>
	<div class="wrapper">
		<?php include 'sidebar.php';
		include './api/graph.php';
		?>
		<div class="main">
			<?php include 'navbar.php'; ?>
			<main class="content" style="padding :15px;">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-12 col-xl-9">
							<div class="row">
								<div class="col-xl-12">
									<p> <img src="./img/icons/calendar.png" style="width:25px;"> &nbsp; Filter : <?php
																													if ($_SESSION["day"] == 1) {
																														echo '<a href="#"><span class="badge bg-info">1 Day</span></a>
									<a href="#" onclick="submitResult(event , 7)"><span class="badge bg-secondary">7 Day</span></a>
									<a href="#" onclick="submitResult(event , 15)"><span class="badge bg-secondary">15 Days</span></a>
									<a href="#" onclick="submitResult(event , 30)"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 7) {
																														echo '<a href="#" onclick="submitResult(event , 1)"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="#"><span class="badge bg-warning">7 Days</span></a>
									<a href="#" onclick="submitResult(event , 15)"><span class="badge bg-secondary">15 Days</span></a>
									<a href="#" onclick="submitResult(event , 30)"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 15) {
																														echo '<a href="#" onclick="submitResult(event , 1)"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="#" onclick="submitResult(event , 7)"><span class="badge bg-secondary">7 Days</span></a>
									<a href="#"><span class="badge bg-success">15 Days</span></a>
									<a href="#" onclick="submitResult(event , 30)"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 30) {
																														echo '<a href="#" onclick="submitResult(event , 1)"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="#" onclick="submitResult(event , 7)"><span class="badge bg-secondary">7 Days</span></a>
									<a href="#" onclick="submitResult(event , 15)"><span class="badge bg-secondary">15 Days</span></a>
									<a href="#"><span class="badge bg-primary">30 Days</span></a>';
																													}
																													?></p>
									<a onclick="call_data()">
										<p><img src="./img/icons/14.png" style="width:25px; margin-right:10px;" alt=""><span class="badge bg-info">Refresh Data<span id="demo" style="font-size: 10px;"></span></span></p>
									</a>
								</div>
							</div>
							<hr />
							<h1 class="h3 mb-3"><strong>Dashboard</strong></h1>

							<div class="row">
								<div class="col-12 col-md-3">
									<div class="card text-white bg-danger mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $critical ?></h4>
												<h5 class="card-title text-light">Critical Severity</h5>
											</center>
										</div>
									</div>

									<!-- ---------------------------- -->
									<button class="btn btn-outline-danger" data-toggle="modal" data-target=".data-blocked-critical" style="width:100%;">
										<p style="font-size:11px; color:gray; margin:0px;">Blocked</p> <?php echo $critical_blocked; ?>
									</button>
									<!-- ---------------------------- -->
									<div class="modal fade data-blocked-critical" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>Critical Severity</h3>
												<h5>Action : Blocked</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($critical_blocked_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- ---------------------------- -->
									<button class="btn btn-outline-danger mt-2" style="width:100%;" data-toggle="modal" data-target=".data-detected-critical">
										<p style="font-size:11px; color:gray; margin:0px;">Detected</p> <?php echo $critical_detected; ?>
									</button>
									<!-- ---------------------------- -->
									<div class="modal fade data-detected-critical" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>Critical Severity</h3>
												<h5>Action : Detected</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($critical_detected_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- ---------------------------- -->
								</div>
								<div class="col-12 col-md-3">
									<div class="card text-white bg-warning mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $high; ?></h4>
												<h5 class="card-title text-light">High Severity</h5>
											</center>
										</div>
									</div>

									<!-- ---------------------------- -->
									<button class="btn btn-outline-warning" data-toggle="modal" data-target=".data-blocked-high" style="width:100%;">
										<p style="font-size:11px; color:gray; margin:0px;">Blocked</p> <?php echo $high_blocked; ?>
									</button>
									<!-- ---------------------------- -->
									<div class="modal fade data-blocked-high" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>High Severity</h3>
												<h5>Action : Blocked</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($high_blocked_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- ---------------------------- -->

									<button class="btn btn-outline-warning mt-2" style="width:100%;" data-toggle="modal" data-target=".data-detected-high">
										<p style="font-size:11px; color:gray; margin:0px;">Detected</p> <?php echo $high_detected; ?>
									</button>

									<!-- ---------------------------- -->
									<div class="modal fade data-detected-high" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>High Severity</h3>
												<h5>Action : Detected</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($high_detected_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- ---------------------------- -->
								</div>
								<div class="col-12 col-md-3">
									<div class="card text-white bg-primary mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $medium; ?></h4>
												<h5 class="card-title text-light">Medium Severity</h5>
											</center>
										</div>
									</div>
									<!-- ---------------------------- -->

									<button class="btn btn-outline-primary" style="width:100%;" data-toggle="modal" data-target=".data-blocked-medium">
										<p style="font-size:11px; color:gray; margin:0px;">Blocked</p> <?php echo $medium_blocked; ?>
									</button>
									<!-- ---------------------------- -->
									<div class="modal fade data-blocked-medium" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>Medium Severity</h3>
												<h5>Action : Blocked</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($medium_blocked_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- ---------------------------- -->
									<button class="btn btn-outline-primary mt-2" style="width:100%;" data-toggle="modal" data-target=".data-detected-medium">
										<p style="font-size:11px; color:gray; margin:0px;">Detected</p> <?php echo $medium_detected; ?>
									</button>
									<!-- ---------------------------- -->
									<div class="modal fade data-detected-medium" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>Medium Severity</h3>
												<h5>Action : Detected</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($medium_detected_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- ---------------------------- -->
								</div>
								<div class="col-12 col-md-3">
									<div class="card text-white bg-success mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $low; ?></h4>
												<h5 class="card-title text-light">Low Severity</h5>
											</center>
										</div>
									</div>
									<!-- ---------------------------- -->
									<button class="btn btn-outline-success" style="width:100%;" data-toggle="modal" data-target=".data-detected-low">
										<p style="font-size:11px; color:gray; margin:0px;">Blocked</p> <?php echo $low_blocked; ?>
									</button>
									<!-- ---------------------------- -->
									<div class="modal fade data-blocked-low" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>Low Severity</h3>
												<h5>Action : Blocked</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($low_blocked_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>

									<!-- ---------------------------- -->
									<button class="btn btn-outline-success mt-2" style="width:100%;" data-toggle="modal" data-target=".data-detected-low">
										<p style="font-size:11px; color:gray; margin:0px;">Detected</p> <?php echo $low_detected; ?>
									</button>
									<!-- ---------------------------- -->
									<div class="modal fade data-detected-low" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content p-4">
												<h3>Low Severity</h3>
												<h5>Action : Detected</h5>
												<table class="table mt-3" style="font-size: 14px;">
													<thead>
														<tr>
															<th scope="col">#</th>
															<th scope="col">name</th>
														</tr>
													</thead>
													<tbody>
														<?php
														foreach ($low_detected_data as $key => $value) {
														?>
															<tr>
																<td><?php echo $value['id'] ?></td>
																<td><?php echo $value['rawName'] ?></td>
															</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!-- ---------------------------- -->
								</div>

								<?php if ($unknown > 0) { ?>
									<div class="col-12 mt-4">
										<div class="card text-white bg-secondary">
											<div class="card-body">
												<center>
													<h4 class="text-light" style="font-size:50px;"><?php echo $unknown; ?></h4>
													<h5 class="card-title text-light">Unknown Severity</h5>
												</center>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>

							<div class="row mt-4">
								<div class="col-12 col-md-6">
									<div class="card flex-fill w-100" style="height: 365px;">
										<div class="card-header" style="padding-bottom: 0px;">
											<h5 class="card-title">Summary Actions </h5>
										</div>
										<div class="card-body d-flex">
											<div class="align-self-center w-100">
												<div class="row">
													<div class="col-12 col-md-12">
														<div class="chart chart-xs">
															<canvas id="chartjs-dashboard-pie"></canvas>
														</div>
													</div>
												</div>
												<br />
												<div class="row">
													<div class="col-6 col-md-6 hide">
														<table class="table mb-0">
															<tbody>
																<tr>
																	<button class="btn btn-outline-success" style="width:100%;" disabled>
																		<p style="font-size:11px; color:gray; margin:0px;">&nbsp;&nbsp;Blocked </p><?php echo $BLOCKED; ?>
																	</button>
																</tr>
															</tbody>
														</table>
													</div>

													<div class="col-6 col-md-6 hide">
														<table class="table mb-0">
															<tbody>
																<tr>
																	<button class="btn btn-outline-danger" style="width:100%;" disabled>
																		<p style="font-size:11px; color:gray; margin:0px;">&nbsp;&nbsp;Detected </p><?php echo $DETECTED; ?>
																	</button>
																</tr>
															</tbody>
														</table>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="card flex-fill w-100" style="height: 365px;">
										<div class="card-header" style="padding-bottom: 0px;">
											<h5 class="card-title">Summary Incidents</h5>
										</div>
										<div class="card-body d-flex">
											<div class="align-self-center w-100">
												<div class="row">
													<div class="col-12 col-md-12">
														<div class="chart chart-xs">
															<canvas id="chartjs-pie"></canvas>
														</div>
													</div>
												</div>
												<br />
												<div class="row">
													<div class="col-12 col-md-12 hide">
														<table class="table mb-0">
															<tbody>
																<tr>
																	<button class="btn btn-outline-dark" style="width:100%;" disabled>
																		<p style="font-size:11px; color:gray; margin:0px;">&nbsp;&nbsp;Total Incident </p><?php echo $medium + $high + $critical + $low; ?>
																	</button>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-12 col-md-4">
									<div class="card" style="height:280px;">
										<div class="card-body">
											<div class="row">
												<div class="col mt-0">
													<h5 class="card-title">Top Alerted Policies</h5>
												</div>
												<div class="col-auto">
													<div class="stat text-primary">
														<i class="align-middle" data-feather="bell"></i>
													</div>
												</div>
											</div>
											<br />
											<div>
												<?php
												foreach ($topcountplaybook as $key => $val) {
													if ($key < 5) {
														echo '<button class="btn btn-outline-primary mb-2" style="width: 100%; text-align: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" disabled>
														<p style="font-size: 11px; color: black; margin: 0px;">' . $val . '</p>
													</button>';
													}
												}
												?>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-md-4">
									<div class="card" style="height: 280px;">
										<div class="card-body">
											<div class="row">
												<div class="col mt-0">
													<h5 class="card-title">Alerted Service Connectors</h5>
												</div>

												<div class="col-auto">
													<div class="stat text-primary">
														<i class="align-middle" data-feather="box"></i>
													</div>
												</div>
											</div>
											<br>
											<div class="mb-0">
												<?php
												foreach ($topsourceBrand as $key => $val) {
													if ($key < 5) {
														echo '<button class="btn btn-outline-primary mb-2" style="width: 100%; text-align: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" disabled>
														<p style="font-size: 11px; color: black; margin: 0px;">' . $val . '</p>
													</button>';
													}
												}
												?>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-md-4">
									<div class="card" style="height:280px;">
										<div class="card-body">
											<div class="row">
												<div class="col mt-0">
													<h5 class="card-title">Top Alerted User</h5>
												</div>
												<div class="col-auto">
													<div class="stat text-primary">
														<i class="align-middle" data-feather="user"></i>
													</div>
												</div>
											</div>
											<br>
											<div class="mb-0">
												<?php
												foreach ($topcountUser as $key => $val) {
													if ($key < 5) {
														echo '<button class="btn btn-outline-primary mb-2" style="width: 100%; text-align: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" disabled>
														<p style="font-size: 11px; color: black; margin: 0px;">' . $val . '</p>
													</button>';
													}
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-xl-3">
							<div class="card flex-fill" style="position: absolute; top: 0; bottom: 0; left: 0; right: 0; overflow-y: auto;">
								<div class="card-header" style="padding-right: 10px; padding-left: 10px;">
									<h5 class="card-title" style="font-size: 12px; margin: 0px;">Threat Intelligence News </h5>
									<hr style="margin-top: 5px; margin-bottom: 5px;" />
									<ul style="font-size: 11px; padding: 0px;">
										<?php include 'feed.php'; ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
			</main>
			<?php include './footer.php'; ?>
		</div>
	</div>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<!-- <script src="js/app.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
	<script>
		var tmp = null;

		function call_data(day) {
			$.ajax({
				'async': false,
				'url': './api/get_number_incident.php?day=' + day,
				'global': true,
				'type': 'POST',
				'success': function(result) {
					console.log(result);
					tmp = result;
				},
				'data': {
					'request': "",
					'target': 'arrange_url',
					'method': 'method_target'
				},
				'error': function() {
					console.log('error');
				}
			});
		};
	</script>
	<script>
		function submitResult(e, day) {
			call_data(day);
			var countincident = tmp;
			e.preventDefault();

			Swal.fire({
				title: 'แจ้งเตือนจากระบบ',
				text: "ข้อมูลมีจำนวน " + countincident + " อาจใช้เวลานานในการดึงข้อมูล",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'ยืนยัน',
				cancelButtonText: "ยกเลิก"
			}).then((result) => {
				if (result.isConfirmed) {
					window.location.href = './api/api-day.php?select=' + day + '&page=home';
					let timerInterval
					Swal.fire({
						title: 'กำลังนำเข้าข้อมูล!',
						html: 'I will close in <b></b> milliseconds.',
						timer: tmp * 2,
						timerProgressBar: true,
						didOpen: () => {
							Swal.showLoading()
							const b = Swal.getHtmlContainer().querySelector('b')
							timerInterval = setInterval(() => {
								b.textContent = Swal.getTimerLeft()
							}, 100)
						},
						willClose: () => {
							clearInterval(timerInterval)
						}
					}).then((result) => {
						/* Read more about handling dismissals below */
						if (result.dismiss === Swal.DismissReason.timer) {
							console.log('I was closed by the timer')
						}
					})
				}
			})
		}
	</script>

	<script>
		var x = setInterval(function() {
			var d = new Date(); //get current time
			var seconds = d.getMinutes() * 60 + d.getSeconds(); //convet current mm:ss to seconds for easier caculation, we don't care hours.
			var fiveMin = 60 * 5; //five minutes is 300 seconds!
			var timeleft = fiveMin - seconds % fiveMin; // let's say now is 01:30, then current seconds is 60+30 = 90. And 90%300 = 90, finally 300-90 = 210. That's the time left!
			var result = parseInt(timeleft / 60) + ':' + timeleft % 60; //formart seconds back into mm:ss 
			document.getElementById('demo').innerHTML = "   (Automatic data updates in : " + result + " minutes)";
			if (result == "0:1") {
				window.location.href = "./api/api-redata.php?page=home";
			}
		}, 1000);
	</script>

	<script>
		document.addEventListener("DOMContentLoaded", function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-pie"), {
				type: "pie",
				data: {
					labels: ["Medium Severity", "High Severity", "Critical Severity", "Low Severity"],
					datasets: [{
						data: [<?php echo $medium; ?>, <?php echo $high; ?>, <?php echo $critical; ?>, <?php echo $low; ?>],
						backgroundColor: [
							window.theme.primary,
							window.theme.warning,
							window.theme.danger,
							window.theme.success,
						],
						borderColor: "transparent"
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					}
				}
			});
		});
	</script>

	<script>
		document.addEventListener("DOMContentLoaded", function() {
			// Pie chart
			new Chart(document.getElementById("chartjs-dashboard-pie"), {
				type: "pie",
				data: {
					labels: ["DETECTED", "BLOCKED"],
					datasets: [{
						data: [<?php echo  $DETECTED . "," . $BLOCKED ?>],
						backgroundColor: [
							window.theme.danger,
							window.theme.success
						],
						borderWidth: 4
					}]
				},
				options: {
					responsive: !window.MSInputMethodContext,
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					cutoutPercentage: 75
				}
			});
		});
	</script>

</body>

</html>