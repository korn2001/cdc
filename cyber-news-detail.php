<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<link rel="canonical" href="https://demo-basic.adminkit.io/" />
	<title>MDR Center</title>
	<link href="css/app.css" rel="stylesheet">
</head>

<body>
	<div class="wrapper">
		<?php include 'sidebar.php'; ?>
		<div class="main">
			<?php include 'navbar.php'; ?>
			<main class="content" style="padding :15px;">
				<div class="container-fluid p-0">
					<div class="row">
						<?php
						include("./api/config-gp.php");
						$sql = "SELECT * FROM tbl_news WHERE id=" . $_GET['id'];
						$result = $conn->query($sql);
						// var_dump($result);
						if ($result->num_rows > 0) {
							while ($row = $result->fetch_assoc()) {
						?>
								<div class="card mb-3" >
									<!-- <img class="card-img-top" src="./img/uploads/<?php echo $row["img"] ?>" style="object-fit: cover; "> -->
									<div class="card-body">
										<img class="card-img-top mt-2 mb-4" src="./img/uploads/malware.webp" style="object-fit: cover; height:500px;">
										<!-- <img class="card-img-top mt-2 mb-4" src="./img/uploads/<?php echo $row["img"] ?>" style="object-fit: cover; height:500px;"> -->
										<br />
										<h5 style="font-size: 18px;" class="card-title"><?php echo $row["name"] ?></h5>
										<br />
										<p style="font-size: 14px;" class="card-text">&emsp;&emsp;<?php echo $row["detail1"] ?></p>
										<p style="font-size: 14px;" class="card-text">&emsp;&emsp;<?php echo $row["detail2"] ?></p>
										<p style="font-size: 14px;" class="card-text">&emsp;&emsp;<?php echo $row["detail3"] ?></p>
										<p class="card-text"><small class="text-muted">by <?php echo $row["user"] ?> <br /> Last updated : <?php echo $row["date"]  ?><br />Reference : <a href="<?php echo $row["ref"] ?>"><?php echo $row["ref"] ?></a></small></p>
									</div>
								</div>
						<?php
							}
						}
						?>
					</div>
				</div>
				<?php include './footer.php'; ?>
		</div>
	</div>
</body>
<script src="js/app.js"></script>

</html>