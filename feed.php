<?php
include("./api/config-gp.php");
$sql = "SELECT * FROM tbl_news ORDER BY id DESC LIMIT 15";
$result = $conn->query($sql);
echo '<h5 class="card-title mb-3 mt-3" style="font-size: 12px;"> &nbsp;&nbsp; <i class="align-middle" data-feather="layout"></i> Growpro consulting & services</h5> ';

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
?>
        <div class="row">
            <div class="col-5">
                <!-- <img class="card-img" src="./img/uploads/<?php echo $row["img"] ?>" style="object-fit: cover; width:100%;"> -->
                <img src="<?php echo getRandomImage(); ?>" alt="Random Image" style="object-fit: cover; width:100%;">
            </div>
            <div class="col-7">
                <?php echo implode(' ', array_slice(explode(' ', $row["name"]), 0, 10)) . "..."; ?>
                <br />
                <small style="color:gray;"> by <?php echo $row["user"] ?></small>

            </div>
        </div>
        <hr />
<?php
    }
}
?>