<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<link rel="canonical" href="https://demo-basic.adminkit.io/" />
	<title>MDR Center</title>
	<link href="css/app.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
	<style>
		@media screen and (max-width: 425px) {
			.hidden_mobile {
				visibility: hidden;
				clear: both;
				float: left;
				margin: 10px auto 5px 20px;
				width: 28%;
				display: none;
			}
		}
	</style>
</head>

<body>
	<div class="wrapper">
		<?php include 'sidebar.php'; ?>
		<div class="main">
			<?php include 'navbar.php'; ?>
			<?php require("api/endpoint.php"); ?>
			<?php include './api/showendpoint-number.php' ?>
			<main class="content" style="padding :15px;">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-xl-12">
							<br />
							<a href="endpoint.php">
								<p><img src="./img/icons/14.png" style="width:25px; margin-right:10px;" alt=""><span class="badge bg-info">Refresh Data<span id="demo" style="font-size: 10px;"></span></span></p>
							</a>

						</div>
					</div>
					<br />
					<h1 class="h3 mb-3"><strong>Endpoint</strong></h1>
					<div class="row">
						<div class="col-xs-12 col-md-3">
							<div class="card text-white bg-info mb-4">
								<div class="card-body">
									<center>
										<h1 class="text-light" style="font-size: 50px;"><?php echo $endpoint_connected + $endpoint_disconnected + $endpoint_lost; ?></h1>
										<h5 class="card-title text-light">Total</h5>
									</center>
								</div>

							</div>
						</div>

						<div class="col-xs-12 col-md-2">
							<div class="card text-white bg-success mb-4">
								<div class="card-body">
									<center>
										<h1 class="text-light" style="font-size: 50px;"><?php echo $endpoint_connected; ?></h1>
										<h5 class="card-title text-light">Connected</h5>
									</center>
								</div>

							</div>
						</div>
						<div class="col-xs-12 col-md-2 hidden_mobile">
							<div class="card text-white bg-secondary mb-4">
								<div class="card-body">
									<center>
										<h1 class="text-light" style="font-size: 50px;"><?php echo $endpoint_disconnected; ?></h1>
										<h5 class="card-title text-light">Disconnected</h5>
									</center>
								</div>

							</div>
						</div>
						<div class="col-xs-12 col-md-2 hidden_mobile">
							<div class="card text-white bg-warning mb-4">
								<div class="card-body">
									<center>
										<h1 class="text-light" style="font-size: 50px;"><?php echo $endpoint_lost; ?></h1>
										<h5 class="card-title text-light">Lost</h5>
									</center>
								</div>

							</div>
						</div>
						<div class="col-xs-12 col-md-3">
							<div class="card text-white bg-danger mb-4">
								<div class="card-body">
									<center>
										<h1 class="text-light" style="font-size: 50px;"><?php echo $endpoint_isolate; ?></h1>
										<h5 class="card-title text-light">Isolate</h5>
									</center>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-lg-12 col-xxl-12 d-flex">
							<div class="card flex-fill" style="overflow-x:auto;">
								<br />
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item" role="presentation">
										<button class="nav-link active" id="all-tab" data-bs-toggle="tab" data-bs-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true" style="color:#14a2b8;">Endpoint Info</button>
									</li>
									<li class="nav-item" role="presentation">
										<button class="nav-link" id="xdr-tab" data-bs-toggle="tab" data-bs-target="#xdr" type="button" role="tab" aria-controls="home" aria-selected="true" style="color:#14a2b8;">Endpoint Action</button>
									</li>
								</ul>

								<div class="tab-content">
									<div class="tab-pane active" id="all" role="tabpanel" aria-labelledby="all-tab" style="margin:20px;">
										<table id="endpoint-all" class="table table-hover my-0" style="font-size: 14px;">
											<thead>
												<tr>
													<th class="hidden_mobile" style="width:5%;">ID</th>
													<th style="width:20%;">Endpoint Name</th>
													<th class="hidden_mobile" style="width:20%;">Endpoint Type</th>
													<th class="hidden_mobile" style="width:20%;">Operating System</th>
													<th class="hidden_mobile" style="width:20%;">Endpoint Status</th>
													<th class="hidden_mobile" style="width:20%;">Last Seen</th>
												</tr>
											</thead>
											<tbody>
												<?php include './api/showendpoint-table.php'; ?>
											</tbody>
										</table>
									</div>

									<div class="tab-pane" id="xdr" role="tabpanel" aria-labelledby="xdr-tab" style="margin:20px;">
										<table id="endpoint-action" class="table table-hover my-0" style="font-size: 14px;">
											<thead>
												<tr>
													<th class="hidden_mobile" style="width:5%;">ID</th>
													<th style="width:20%;">Endpoint Name</th>
													<th class="hidden_mobile" style="width:20%;">Endpoint Status</th>
													<th style="width:20%;">Isolate Status</th>
													<th style="width:20%;">Scan Status</th>

												</tr>
											</thead>
											<tbody>
												<?php include './api/showendpoint-table-action.php'; ?>
											</tbody>
										</table>
									</div>

								</div>

							</div>
						</div>
					</div>

				</div>
			</main>
			<?php include './footer.php'; ?>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
	<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="js/app.js"></script>
	<script>
		$(document).ready(function() {
			$('#endpoint-all').DataTable({
				columnDefs: [{
						targets: [0],
						orderData: [0, 1],
					},
					{
						targets: [1],
						orderData: [1, 0],
					},
					{
						targets: [4],
						orderData: [4, 0],
					},
				],
				aLengthMenu: [
					[25, 50, 100, 200, -1],
					[25, 50, 100, 200, "All"]
				],
				iDisplayLength: 100
			});

			$('#endpoint-action').DataTable({
				columnDefs: [{
						targets: [0],
						orderData: [0, 1],
					},
					{
						targets: [1],
						orderData: [1, 0],
					}
				],
				aLengthMenu: [
					[25, 50, 100, 200, -1],
					[25, 50, 100, 200, "All"]
				],
				iDisplayLength: 100
			});

		});

		var x = setInterval(function() {
			var d = new Date(); //get current time
			var seconds = d.getMinutes() * 60 + d.getSeconds(); //convet current mm:ss to seconds for easier caculation, we don't care hours.
			var fiveMin = 60 * 5; //five minutes is 300 seconds!
			var timeleft = fiveMin - seconds % fiveMin; // let's say now is 01:30, then current seconds is 60+30 = 90. And 90%300 = 90, finally 300-90 = 210. That's the time left!
			var result = parseInt(timeleft / 60) + ':' + timeleft % 60; //formart seconds back into mm:ss 
			document.getElementById('demo').innerHTML = "   (Automatic data updates in : " + result + " minutes)";
			if (result == "0:1") {
				window.location.href = "endpoint.php";
			}
		}, 1000);
	</script>

</body>

</html>