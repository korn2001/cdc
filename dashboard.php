<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<title>MDR Center</title>
	<link href="css/app.css" rel="stylesheet">
	<style>
		@media screen and (min-width: 0px) and (max-width: 767px) {
			.hide {
				display: none;
			}
		}
	</style>
</head>
<?php include "loader.php"; ?>

<body>
	<div class="wrapper">
		<?php include 'sidebar.php'; ?>
		<div class="main">
			<?php include 'navbar.php'; ?>
			<?php include './api/sum-dashboard.php'; ?>
			<main class="content" style="padding :15px;">
				<div class="container-fluid p-0">

					<div class="row">
						<div class="col-12 col-xl-12">
							<div class="row">
								<div class="col-xl-12">
									<p> <img src="./img/icons/calendar.png" style="width:25px;"> &nbsp; Filter : <?php
																													if ($_SESSION["day"] == 1) {
																														echo '<a href="#"><span class="badge bg-info">1 Day</span></a>
									<a href="#" onclick="submitResult(event , 7)"><span class="badge bg-secondary">7 Day</span></a>
									<a href="#" onclick="submitResult(event , 15)"><span class="badge bg-secondary">15 Days</span></a>
									<a href="#" onclick="submitResult(event , 30)"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 7) {
																														echo '<a href="#" onclick="submitResult(event , 1)"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="#"><span class="badge bg-warning">7 Days</span></a>
									<a href="#" onclick="submitResult(event , 15)"><span class="badge bg-secondary">15 Days</span></a>
									<a href="#" onclick="submitResult(event , 30)"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 15) {
																														echo '<a href="#" onclick="submitResult(event , 1)"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="#" onclick="submitResult(event , 7)"><span class="badge bg-secondary">7 Days</span></a>
									<a href="#"><span class="badge bg-success">15 Days</span></a>
									<a href="#" onclick="submitResult(event , 30)"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 30) {
																														echo '<a href="#" onclick="submitResult(event , 1)"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="#" onclick="submitResult(event , 7)"><span class="badge bg-secondary">7 Days</span></a>
									<a href="#" onclick="submitResult(event , 15)"><span class="badge bg-secondary">15 Days</span></a>
									<a href="#"><span class="badge bg-primary">30 Days</span></a>';
																													}
																													?></p>
									<a onclick="call_data()">
										<p><img src="./img/icons/14.png" style="width:25px; margin-right:10px;" alt=""><span class="badge bg-info">Refresh Data<span id="demo" style="font-size: 10px;"></span></span></p>
									</a>
								</div>
							</div>

							<div class="row">
								<div class="col-12 col-lg-12">
									<div class="card">
										<div class="card-header">
											<h5 class="card-title mt-2">Incident Summary</h5>
										</div>
										<div class="card-body">
											<div class="chart">
												<canvas id="chartjs-bar"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-12 col-md-3">
									<div class="card text-white bg-danger mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $sum_critical ?></h4>
												<h5 class="card-title text-light">Critical Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- ---------------------------- -->

								<div class="col-12 col-md-3">
									<div class="card text-white bg-warning mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $sum_high; ?></h4>
												<h5 class="card-title text-light">High Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- ---------------------------- -->

								<div class="col-12 col-md-3">
									<div class="card text-white bg-primary mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $sum_medium; ?></h4>
												<h5 class="card-title text-light">Medium Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- ---------------------------- -->

								<div class="col-12 col-md-3">
									<div class="card text-white bg-success mb-2">
										<div class="card-body">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $sum_low; ?></h4>
												<h5 class="card-title text-light">Low Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- ---------------------------- -->

								<?php if ($unknown > 0) { ?>
									<div class="col-12 mt-4">
										<div class="card text-white bg-secondary">
											<div class="card-body">
												<center>
													<h4 class="text-light" style="font-size:50px;"><?php echo $unknown; ?></h4>
													<h5 class="card-title text-light">Unknown Severity</h5>
												</center>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>

							<br />
							<h1 class="h3 mb-3"><strong>Tenant</strong></h1>
							<div class="row">

								<!-- start hospital -->
								<div class="col-md-3">
									<div class="card" style="height:200px; display: flex; flex-direction: column; justify-content: center; align-items: center;">
										<img src="./img/icons/labs.png" class="card-img-top" style="width: 50%; padding-top: 35px;">
										<div class="card-body">
											<h3 class="card-title" style="text-align: center;">Ratchaburi</h3>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="card">
										<canvas id="myChart-Ratchaburi" style="width:100%; height:200px; padding:20px;"></canvas>
									</div>
								</div>

								<div class="col-md-4">
									<div class="card bg-dark">
										<div class="card-body" style="height:200px; display: flex; justify-content: center; align-items: center; padding: 20px;">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $ratchaburi_all ?></h4>
												<h5 class="card-title text-light">Total Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- stop hospital -->

								<!-- start hospital -->
								<div class="col-md-3">
									<div class="card" style="height:200px; display: flex; flex-direction: column; justify-content: center; align-items: center;">
										<img src="./img/icons/labs.png" class="card-img-top" style="width: 50%; padding-top: 35px;">
										<div class="card-body">
											<h3 class="card-title" style="text-align: center;">Chonburi</h3>
										</div>
									</div>
								</div>

								<div class="col-md-5">
									<div class="card">
										<canvas id="myChart-Chonburi" style="width:100%; height:200px; padding:20px;"></canvas>
									</div>
								</div>

								<div class="col-md-4">
									<div class="card bg-dark">
										<div class="card-body" style="height:200px; display: flex; justify-content: center; align-items: center;">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $chonburi_all ?></h4>
												<h5 class="card-title text-light">Total Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- stop hospital -->

								<!-- start hospital -->
								<div class="col-md-3">
									<div class="card" style="height:200px; display: flex; flex-direction: column; justify-content: center; align-items: center;">
										<img src="./img/icons/labs.png" class="card-img-top" style="width: 50%; padding-top: 35px;">
										<div class="card-body">
											<h3 class="card-title" style="text-align: center;">Neurological Institute</h3>
										</div>
									</div>
								</div>

								<div class="col-md-5">
									<div class="card">
										<canvas id="myChart-NeurologicalInstitute" style="width:100%; height:200px; padding:20px;"></canvas>
									</div>
								</div>

								<div class="col-md-4">
									<div class="card bg-dark">
										<div class="card-body" style="height:200px; display: flex; justify-content: center; align-items: center;">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $neurologicalinstitute_all ?></h4>
												<h5 class="card-title text-light">Total Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- stop hospital -->

								<!-- start hospital -->
								<div class="col-md-3">
									<div class="card" style="height:200px; display: flex; flex-direction: column; justify-content: center; align-items: center;">
										<img src="./img/icons/labs.png" class="card-img-top" style="width: 50%; padding-top: 35px;">
										<div class="card-body">
											<h3 class="card-title" style="text-align: center;">Maharat Nakhon</h3>
										</div>
									</div>
								</div>

								<div class="col-md-5">
									<div class="card">
										<canvas id="myChart-MaharatNakhon" style="width:100%; height:200px; padding:20px;"></canvas>
									</div>
								</div>

								<div class="col-md-4">
									<div class="card bg-dark">
										<div class="card-body" style="height:200px; display: flex; justify-content: center; align-items: center;">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $maharatnakhon_all ?></h4>
												<h5 class="card-title text-light">Total Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- stop hospital -->

								<!-- start hospital -->
								<div class="col-md-3">
									<div class="card" style="height:200px; display: flex; flex-direction: column; justify-content: center; align-items: center;">
										<img src="./img/icons/labs.png" class="card-img-top" style="width: 50%; padding-top: 35px;">
										<div class="card-body">
											<h3 class="card-title" style="text-align: center;">Hat Yai</h3>
										</div>
									</div>
								</div>

								<div class="col-md-5">
									<div class="card">
										<canvas id="myChart-HatYai" style="width:100%; height:200px; padding:20px;"></canvas>
									</div>
								</div>

								<div class="col-md-4">
									<div class="card bg-dark">
										<div class="card-body" style="height:200px; display: flex; justify-content: center; align-items: center;">
											<center>
												<h4 class="text-light" style="font-size:50px;"><?php echo $hatyai_all ?></h4>
												<h5 class="card-title text-light">Total Severity</h5>
											</center>
										</div>
									</div>
								</div>
								<!-- stop hospital -->

							</div>
						</div>
					</div>
				</div>
			</main>
			<?php include './footer.php'; ?>
		</div>
	</div>
	<script src="js/app.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
	<script>
		document.addEventListener("DOMContentLoaded", function() {

			var critical_data = <?php echo "[" . $ratchaburi_critical . "," . $chonburi_critical . "," . $neurologicalinstitute_critical . "," . $maharatnakhon_critical . "," . $hatyai_critical . "]" ?>;
			var high_data = <?php echo "[" . $ratchaburi_high . "," . $chonburi_high . "," . $neurologicalinstitute_high . "," . $maharatnakhon_high . "," . $hatyai_high . "]" ?>;
			var medium_data = <?php echo "[" . $ratchaburi_medium . "," . $chonburi_medium . "," . $neurologicalinstitute_medium . "," . $maharatnakhon_medium . "," . $hatyai_medium . "]" ?>;
			var low_data = <?php echo "[" . $ratchaburi_low . "," . $chonburi_low . "," . $neurologicalinstitute_low . "," . $maharatnakhon_low . "," . $hatyai_low . "]" ?>;
			var all_data = <?php echo "[" . $ratchaburi_all . "," . $chonburi_all . "," . $neurologicalinstitute_all . "," . $maharatnakhon_all . "," . $hatyai_all . "]" ?>;

			new Chart(document.getElementById("chartjs-bar"), {
				type: "bar",
				data: {
					labels: ["Ratchaburi", "Chonburi", "Neurological-Institute", "Maharat Nakhon", "Hat Yai"],
					datasets: [{
						label: "All",
						backgroundColor: window.theme.muted,
						borderColor: window.theme.muted,
						hoverBackgroundColor: window.theme.muted,
						hoverBorderColor: window.theme.muted,
						data: all_data,
						barPercentage: .75,
						categoryPercentage: .5
					}, {
						label: "Critical",
						backgroundColor: window.theme.danger,
						borderColor: window.theme.danger,
						hoverBackgroundColor: window.theme.danger,
						hoverBorderColor: window.theme.danger,
						data: critical_data,
						barPercentage: .75,
						categoryPercentage: .5
					}, {
						label: "High",
						backgroundColor: window.theme.warning,
						borderColor: window.theme.warning,
						hoverBackgroundColor: window.theme.warning,
						hoverBorderColor: window.theme.warning,
						data: high_data,
						barPercentage: .75,
						categoryPercentage: .5
					}, {
						label: "Medium",
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						data: medium_data,
						barPercentage: .75,
						categoryPercentage: .5
					}, {
						label: "Low",
						backgroundColor: window.theme.success,
						borderColor: window.theme.success,
						hoverBackgroundColor: window.theme.success,
						hoverBorderColor: window.theme.success,
						data: low_data,
						barPercentage: .75,
						categoryPercentage: .5
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							gridLines: {
								display: false
							},
							stacked: false,
							ticks: {
								stepSize: 20
							}
						}],
						xAxes: [{
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			});
		});
	</script>

	<script>
		var $ratchaburi_chart = <?php echo "[" . $ratchaburi_critical . "," . $ratchaburi_high . "," . $ratchaburi_medium . "," . $ratchaburi_low . "]" ?>;

		var xValues = ["Critical", "High", "Medium", "Low"];
		var yValues = $ratchaburi_chart;
		var barColors = [window.theme.danger, window.theme.warning, window.theme.primary, window.theme.success];

		new Chart("myChart-Ratchaburi", {
			type: "bar",
			data: {
				labels: xValues,
				datasets: [{
					backgroundColor: barColors,
					data: yValues
				}],
			},

			options: {
				legend: {
					display: false
				},
				title: {
					display: true,
					text: "Ratchaburi Incident"
				}
			},
		});
	</script>


	<script>
		var $Chonburi_chart = <?php echo "[" . $chonburi_critical . "," . $chonburi_high . "," . $chonburi_medium . "," . $chonburi_low . "]" ?>;

		var xValues = ["Critical", "High", "Medium", "Low"];
		var yValues = $Chonburi_chart;
		var barColors = [window.theme.danger, window.theme.warning, window.theme.primary, window.theme.success];

		new Chart("myChart-Chonburi", {
			type: "bar",
			data: {
				labels: xValues,
				datasets: [{
					backgroundColor: barColors,
					data: yValues
				}],
			},

			options: {
				legend: {
					display: false
				},
				title: {
					display: true,
					text: "Chonburi Incident"
				}
			},
		});
	</script>

	<script>
		var $NeurologicalInstitute_chart = <?php echo "[" . $neurologicalinstitute_critical . "," . $neurologicalinstitute_high . "," . $neurologicalinstitute_medium . "," . $neurologicalinstitute_low . "]" ?>;

		var xValues = ["Critical", "High", "Medium", "Low"];
		var yValues = $NeurologicalInstitute_chart;
		var barColors = [window.theme.danger, window.theme.warning, window.theme.primary, window.theme.success];

		new Chart("myChart-NeurologicalInstitute", {
			type: "bar",
			data: {
				labels: xValues,
				datasets: [{
					backgroundColor: barColors,
					data: yValues
				}],
			},

			options: {
				legend: {
					display: false
				},
				title: {
					display: true,
					text: "NeurologicalInstitute Incident"
				}
			},
		});
	</script>

	<script>
		var $MaharatNakhon_chart = <?php echo "[" . $maharatnakhon_critical . "," . $maharatnakhon_high . "," . $maharatnakhon_medium . "," . $maharatnakhon_low . "]" ?>;

		var xValues = ["Critical", "High", "Medium", "Low"];
		var yValues = $MaharatNakhon_chart;
		var barColors = [window.theme.danger, window.theme.warning, window.theme.primary, window.theme.success];

		new Chart("myChart-MaharatNakhon", {
			type: "bar",
			data: {
				labels: xValues,
				datasets: [{
					backgroundColor: barColors,
					data: yValues
				}],
			},

			options: {
				legend: {
					display: false
				},
				title: {
					display: true,
					text: "MaharatNakhon Incident"
				}
			},
		});
	</script>

	<script>
		var $HatYai_chart = <?php echo "[" . $hatyai_critical . "," . $hatyai_high . "," . $hatyai_medium . "," . $hatyai_low . "]" ?>;

		var xValues = ["Critical", "High", "Medium", "Low"];
		var yValues = $HatYai_chart;
		var barColors = [window.theme.danger, window.theme.warning, window.theme.primary, window.theme.success];

		new Chart("myChart-HatYai", {
			type: "bar",
			data: {
				labels: xValues,
				datasets: [{
					backgroundColor: barColors,
					data: yValues
				}],
			},

			options: {
				legend: {
					display: false
				},
				title: {
					display: true,
					text: "HatYai Incident"
				}
			},
		});
	</script>

	<script>
		var tmp = null;

		function call_data(day) {
			$.ajax({
				'async': false,
				'url': './api/get_number_incident.php?day=' + day,
				'global': true,
				'type': 'POST',
				'success': function(result) {
					console.log(result);
					tmp = result;
				},
				'data': {
					'request': "",
					'target': 'arrange_url',
					'method': 'method_target'
				},
				'error': function() {
					console.log('error');
				}
			});
		};
	</script>
	<script>
		function submitResult(e, day) {
			call_data(day);
			var countincident = tmp;
			e.preventDefault();

			Swal.fire({
				title: 'แจ้งเตือนจากระบบ',
				text: "ข้อมูลมีจำนวน " + countincident + " อาจใช้เวลานานในการดึงข้อมูล",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'ยืนยัน',
				cancelButtonText: "ยกเลิก"
			}).then((result) => {
				if (result.isConfirmed) {
					window.location.href = './api/api-day.php?select=' + day + '&page=dashboard';
					let timerInterval
					Swal.fire({
						title: 'กำลังนำเข้าข้อมูล!',
						html: 'I will close in <b></b> milliseconds.',
						timer: tmp * 2,
						timerProgressBar: true,
						didOpen: () => {
							Swal.showLoading()
							const b = Swal.getHtmlContainer().querySelector('b')
							timerInterval = setInterval(() => {
								b.textContent = Swal.getTimerLeft()
							}, 100)
						},
						willClose: () => {
							clearInterval(timerInterval)
						}
					}).then((result) => {
						/* Read more about handling dismissals below */
						if (result.dismiss === Swal.DismissReason.timer) {
							console.log('I was closed by the timer')
						}
					})
				}
			})
		}
	</script>

	<script>
		var x = setInterval(function() {
			var d = new Date(); //get current time
			var seconds = d.getMinutes() * 60 + d.getSeconds(); //convet current mm:ss to seconds for easier caculation, we don't care hours.
			var fiveMin = 60 * 5; //five minutes is 300 seconds!
			var timeleft = fiveMin - seconds % fiveMin; // let's say now is 01:30, then current seconds is 60+30 = 90. And 90%300 = 90, finally 300-90 = 210. That's the time left!
			var result = parseInt(timeleft / 60) + ':' + timeleft % 60; //formart seconds back into mm:ss 
			document.getElementById('demo').innerHTML = "   (Automatic data updates in : " + result + " minutes)";
			if (result == "0:1") {
				window.location.href = "./api/api-redata.php?page=dashboard";
			}
		}, 1000);
	</script>

</body>

</html>