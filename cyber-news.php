<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<link rel="canonical" href="https://demo-basic.adminkit.io/" />
	<title>MDR Center</title>
	<link href="css/app.css" rel="stylesheet">
	<style>
		.limit {
			width: 500px;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}

		.line-clamp-3 {
			display: -webkit-box;
			-webkit-line-clamp: 3;
			-webkit-box-orient: vertical;
			overflow: hidden;
			text-overflow: ellipsis;
		}

		.line-clamp-2 {
			display: -webkit-box;
			-webkit-line-clamp: 2;
			-webkit-box-orient: vertical;
			overflow: hidden;
			text-overflow: ellipsis;
		}
	</style>
</head>

<body>
	<div class="wrapper">
		<?php include 'sidebar.php'; ?>
		<?php include 'random-pic.php'; ?>
		<div class="main">
			<?php include 'navbar.php'; ?>
			<main class="content" style="padding :15px;">
				<div class="container-fluid p-0">
					<div class="row">
						<h1 class="h3 mb-3"><strong>Cyber Security News</strong></h1>
						<?php
						include("./api/config-gp.php");
						$sql = "SELECT * FROM tbl_news ORDER BY id DESC LIMIT 80";
						$result = $conn->query($sql);
						if ($result->num_rows > 0) {
							while ($row = $result->fetch_assoc()) {
						?>
								<div class="col-12 col-md-3">
									<div class="card" style="width: 18rem;">
										<!-- <img class="card-img-top" src="./img/uploads/<?php echo $row["img"] ?>" style="height:200px; object-fit: cover;">-->
										<img src="<?php echo getRandomImage();?>" alt="Random Image" style="height:200px; object-fit: cover;">
										<div class="card-body">
											<h5 class="card-title line-clamp-2"><?php echo $row["name"] ?></h5>
											<p class="card-text line-clamp-3" style="font-size: 13px;"><?php echo $row["detail1"] ?></p>
											<p style="font-size: 13px;" > by <?php echo $row["user"] ?></p>
											<a href="cyber-news-detail.php?id=<?php echo $row["id"] ?>" class="btn btn-primary">อ่านเพิ่มเติม</a>
										</div>
									</div>
								</div>
						<?php
							}
						}
						?>
					</div>
				</div>
				<?php include './footer.php'; ?>
		</div>
	</div>
</body>
<script src="js/app.js"></script>

</html>