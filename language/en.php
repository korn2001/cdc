<?php
    $strManagement = "Management";
    $strTenant = "Tenant";
    $strHome = "Home";
    $strDashboard = "Dashboard";
    $strEndpoint = "Endpoint";
    $strIncidentEvent = "Incident & Event";
    $strIncidents = "Incidents";
    $strNews = "News";
    $strThreatIntelligenceNews = "Threat Intelligence News";
    $strCyberSecurityNews = "Cyber Security News";
?>