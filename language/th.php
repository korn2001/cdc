<?php
    $strManagement = "การจัดการ";
    $strTenant = "ผู้เช่า";
    $strHome = "หน้าหลัก";
    $strDashboard = "แผนผังสรุปข้อมูล";
    $strEndpoint = "จุดหมาย";
    $strIncidentEvent = "เหตุการณ์";
    $strIncidents = "เหตุการณ์";
    $strNews = "ข่าวสาร";
    $strThreatIntelligenceNews = "ข่าวภัยคุกคามอัจฉริยะ";
    $strCyberSecurityNews = "ข่าวความมั่นคงทางไซเบอร์";
?>