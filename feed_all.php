<div class="row">
    <button type="button" class="btn btn-dark btn-lg" style="width:98%; margin-left:1%;">Krebsonsecurity</button>
</div>
<br />
<?php
$rss_feed = simplexml_load_file("https://krebsonsecurity.com/feed/");
if (!empty($rss_feed)) {
    $i = 0;
    foreach ($rss_feed->channel->item as $feed_item) {
        if ($i >= 10)
            break;
?>
        <div class="row">
            <h5><?php echo $feed_item->title; ?></h5>
            <br />
            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-info">Report Details</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> Name : <?php echo $feed_item->title; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Source Time Stamp : <?php echo $feed_item->pubDate; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Category : <?php echo $feed_item->category; ?></p>
                </div>
            </div>

            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-warning">Report Description</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> <?php echo implode(' ', array_slice(explode(' ', $feed_item->description), 0, 100)) . "..."; ?></p>
                </div>
            </div>

            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-danger">Report Source</button>
                    <br />
                    <br />
                    <table class="table table-striped" style="font-size:12px; color:gray;">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Source</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="<?php echo $feed_item->link; ?>" target="_blank"><button style="font-size:12px;" type="button" class="btn btn-outline-secondary">Link</button></a></td>
                                <td>
                                    <p style="font-size:12px;">RSS Feed.krebsonsecurity</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr />
<?php
        $i++;
    }
}
?>

<!-- -------------------------------------------------- -->

<div class="row">
    <button type="button" class="btn btn-dark btn-lg" style="width:98%; margin-left:1%;">The Hackers News</button>
</div>
<br />
<?php
$rss_feed = simplexml_load_file("https://feeds.feedburner.com/TheHackersNews?format=xml");
if (!empty($rss_feed)) {
    $i = 0;
    foreach ($rss_feed->channel->item as $feed_item) {
        if ($i >= 10)
            break;
?>
        <div class="row">
            <h5><?php echo $feed_item->title; ?></h5>
            <br />
            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-info">Report Details</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> Name : <?php echo $feed_item->title; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Source Time Stamp : <?php echo $feed_item->pubDate; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Category : <?php echo $feed_item->category; ?></p>
                </div>
            </div>
            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-warning">Report Description</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> <?php echo implode(' ', array_slice(explode(' ', $feed_item->description), 0, 100)) . "..."; ?></p>
                </div>
            </div>

            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-danger">Report Source</button>
                    <br />
                    <br />
                    <table class="table table-striped" style="font-size:12px; color:gray;">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Source</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="<?php echo $feed_item->link; ?>" target="_blank"><button style="font-size:12px;" type="button" class="btn btn-outline-secondary">Link</button></a></td>
                                <td>
                                    <p style="font-size:12px;">RSS Feed.TheHackersNews</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr />
<?php
        $i++;
    }
}
?>



<!-- -------------------------------------------------- -->

<div class="row">
    <button type="button" class="btn btn-dark btn-lg" style="width:98%; margin-left:1%;">Threat Post</button>
</div>
<br />
<?php
$rss_feed = simplexml_load_file("https://threatpost.com/feed/");
if (!empty($rss_feed)) {
    $i = 0;
    foreach ($rss_feed->channel->item as $feed_item) {
        if ($i >= 10)
            break;
?>
        <div class="row">
            <h5><?php echo $feed_item->title; ?></h5>
            <br />
            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-info">Report Details</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> Name : <?php echo $feed_item->title; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Source Time Stamp : <?php echo $feed_item->pubDate; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Category : <?php echo $feed_item->category; ?></p>
                </div>
            </div>
            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-warning">Report Description</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> <?php echo implode(' ', array_slice(explode(' ', $feed_item->description), 0, 100)) . "..."; ?></p>
                </div>
            </div>

            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-danger">Report Source</button>
                    <br />
                    <br />
                    <table class="table table-striped" style="font-size:12px; color:gray;">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Source</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="<?php echo $feed_item->link; ?>" target="_blank"><button style="font-size:12px;" type="button" class="btn btn-outline-secondary">Link</button></a></td>
                                <td>
                                    <p style="font-size:12px;">RSS Feed.threatpost</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr />
<?php
        $i++;
    }
}
?>


<!-- -------------------------------------------------- -->

<div class="row">
    <button type="button" class="btn btn-dark btn-lg" style="width:98%; margin-left:1%;">Feed Burner</button>
</div>
<br />
<?php
$rss_feed = simplexml_load_file("http://feeds.feedburner.com/eset/blog");
if (!empty($rss_feed)) {
    $i = 0;
    foreach ($rss_feed->channel->item as $feed_item) {
        if ($i >= 10)
            break;
?>
        <div class="row">
            <h5><?php echo $feed_item->title; ?></h5>
            <br />
            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-info">Report Details</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> Name : <?php echo $feed_item->title; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Source Time Stamp : <?php echo $feed_item->pubDate; ?></p>
                    <hr>
                    <p style="color:gray; font-size:12px;"> Category : <?php echo $feed_item->category; ?></p>
                </div>
            </div>
            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-warning">Report Description</button>
                    <br />
                    <br />
                    <p style="color:gray; font-size:12px;"> <?php echo implode(' ', array_slice(explode(' ', $feed_item->description), 0, 100)) . "..."; ?></p>
                </div>
            </div>

            <div class="col-4 mb-4">
                <div style="background-color:#FFF; height:300px; padding:20px;">
                    <button type="button" class="btn btn-outline-danger">Report Source</button>
                    <br />
                    <br />
                    <table class="table table-striped" style="font-size:12px; color:gray;">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Source</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="<?php echo $feed_item->link; ?>" target="_blank"><button style="font-size:12px;" type="button" class="btn btn-outline-secondary">Link</button></a></td>
                                <td>
                                    <p style="font-size:12px;">RSS Feed.feedburner</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr />
<?php
        $i++;
    }
}
?>