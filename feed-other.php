
<?php
$rss_feed = simplexml_load_file("https://feeds.feedburner.com/TheHackersNews?format=xml");
echo '<h5 class="card-title mb-3 mt-3" style="font-size: 12px;"> &nbsp;&nbsp; <i class="align-middle" data-feather="layout"></i> TheHackersNews</h5> ';
if (!empty($rss_feed)) {
    $i = 0;
    foreach ($rss_feed->channel->item as $feed_item) {
        if ($i >= 4)
            break;
?>
        <div class="row">
            <div class="col-5">
                <!-- <img class="card-img" src="./img/uploads/1.jpg" style="object-fit: cover; width:100%;"> -->
                <img src="<?php echo getRandomImage(); ?>" alt="Random Image" style="object-fit: cover; width:100%;">

            </div>
            <div class="col-7">
                <?php echo implode(' ', array_slice(explode(' ', $feed_item->description), 0, 14)) . "..."; ?>
                <br />
            </div>
        </div>
        <hr />
    <?php
        $i++;
    }
}

$rss_feed = simplexml_load_file("https://threatpost.com/feed/");
echo '<h5 class="card-title mb-3 mt-3" style="font-size: 12px;"> &nbsp;&nbsp; <i class="align-middle" data-feather="layout"></i> Threatpost</h5> ';
if (!empty($rss_feed)) {
    $i = 0;
    foreach ($rss_feed->channel->item as $feed_item) {
        if ($i >= 4)
            break;
    ?>
        <div class="row">
            <div class="col-5">
                <!-- <img class="card-img" src="./img/uploads/2.jpg" style="object-fit: cover; width:100%;"> -->
                <img src="<?php echo getRandomImage(); ?>" alt="Random Image" style="object-fit: cover; width:100%;">
            </div>
            <div class="col-7">
                <?php echo implode(' ', array_slice(explode(' ', $feed_item->description), 0, 14)) . "..."; ?>
                <br />
            </div>
        </div>
        <hr />
    <?php
        $i++;
    }
}

$rss_feed = simplexml_load_file("http://feeds.feedburner.com/eset/blog");
echo '<h5 class="card-title mb-3 mt-3" style="font-size: 12px;"> &nbsp;&nbsp; <i class="align-middle" data-feather="layout"></i> We Live Security</h5> ';
if (!empty($rss_feed)) {
    $i = 0;
    foreach ($rss_feed->channel->item as $feed_item) {
        if ($i >= 5)
            break;
    ?>
        <div class="row">
            <div class="col-5">
                <img src="<?php echo getRandomImage(); ?>" alt="Random Image" style="object-fit: cover; width:100%;">
                <!-- <img class="card-img" src="./img/uploads/3.jpg" style="object-fit: cover; width:100%;"> -->
            </div>
            <div class="col-7">
                <?php echo implode(' ', array_slice(explode(' ', $feed_item->description), 0, 14)) . "..."; ?>
                <br />
            </div>
        </div>
        <hr />
<?php
        $i++;
    }
}
?>