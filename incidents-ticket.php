<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	
	
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<link rel="canonical" href="https://demo-basic.adminkit.io/" />
	<title>MDR Center</title>
	<link href="css/app.css" rel="stylesheet">
	
	<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
</head>

<style>
		@media screen and (max-width: 425px) {
			.hidden_mobile {
				visibility: hidden;
				clear: both;
				float: left;
				margin: 10px auto 5px 20px;
				width: 28%;
				display: none;
			}
		}
	</style>

<body>
	<div class="wrapper">
		<?php include 'sidebar.php'; ?>
		<?php include './api/graph.php'; ?>
		<div class="main">
			<?php include 'navbar.php'; ?>
			<main class="content" style="padding :15px;">
				<div class="container-fluid p-0">
				<div class="row">
								<div class="col-xl-12">
									<p> <img src="./img/icons/calendar.png" style="width:25px;"> &nbsp; Filter : <?php
																													if ($_SESSION["day"] == 1) {
																														echo '<a href="#"><span class="badge bg-info">1 Day</span></a>
									<a href="./api/api-day.php?select=7&page=incidents-ticket"><span class="badge bg-secondary">7 Day</span></a>
									<a href="./api/api-day.php?select=15&page=incidents-ticket"><span class="badge bg-secondary">15 Days</span></a>
									<a href="./api/api-day.php?select=30&page=incidents-ticket"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 7) {
																														echo '<a href="./api/api-day.php?select=1&page=incidents-ticket"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="#"><span class="badge bg-warning">7 Days</span></a>
									<a href="./api/api-day.php?select=15&page=incidents-ticket"><span class="badge bg-secondary">15 Days</span></a>
									<a href="./api/api-day.php?select=30&page=incidents-ticket"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 15) {
																														echo '<a href="./api/api-day.php?select=1&page=incidents-ticket"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="./api/api-day.php?select=7&page=incidents-ticket"><span class="badge bg-secondary">7 Days</span></a>
									<a href="#"><span class="badge bg-success">15 Days</span></a>
									<a href="./api/api-day.php?select=30&page=incidents-ticket"><span class="badge bg-secondary">30 Days</span></a>';
																													} else if ($_SESSION["day"] == 30) {
																														echo '<a href="./api/api-day.php?select=1&page=incidents-ticket"><span class="badge bg-secondary">1 Day</span></a>
																														<a href="./api/api-day.php?select=7&page=incidents-ticket"><span class="badge bg-secondary">7 Days</span></a>
									<a href="./api/api-day.php?select=15&page=incidents-ticket"><span class="badge bg-secondary">15 Days</span></a>
									<a href="#"><span class="badge bg-primary">30 Days</span></a>';
																													}
																													?></p>
									<a href="./api/api-redata.php?page=incidents-ticket">
										<p><img src="./img/icons/14.png" style="width:25px; margin-right:10px;" alt=""><span class="badge bg-info">Refresh Data<span id="demo" style="font-size: 10px;"></span></span></p>
									</a>
								</div>
							</div>
					<br/>
					<h1 class="h3 mb-3"><strong>Ticket</strong></h1>

					<div class="row">
						<div class="col-12 col-lg-12 col-xxl-12 d-flex">
							<div class="card flex-fill" style="overflow-x:auto;">
								<br/>
								<table id="example" class="table table-hover my-0" style="font-size: 14px;">
									<thead>
										<tr>
											<th style="width:5%;">ID</th>
											<th style="width:40%;">Name</th>
											<th style="width:10%;">Severity</th>
											<th style="width:5%;" class="d-none d-xl-table-cell hidden_mobile">Status</th>
											<th style="width:5%;">Action</th>
											<th style="width:10%;" class="d-none d-xl-table-cell hidden_mobile">Created</th>
											<th style="width:10%;" class="d-none d-md-table-cell hidden_mobile">Closed</th>
										</tr>
									</thead>
									<tbody>
										
										<?php include './api/table-ticket.php'; ?>
										
									</tbody>
								</table>
							</div>
						</div>
						
					</div>

				</div>
			</main>
			<?php include './footer.php'; ?>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
	<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
	<script src="js/app.js"></script>
	<script>
		$(document).ready(function() {
			$('#example').DataTable({
				columnDefs: [{
						targets: [0],
						orderData: [0, 1],
					},
					{
						targets: [1],
						orderData: [1, 0],
					},
					{
						targets: [4],
						orderData: [4, 0],
					},
				],
			});
		});

		var x = setInterval(function() {
			var d = new Date(); //get current time
			var seconds = d.getMinutes() * 60 + d.getSeconds(); //convet current mm:ss to seconds for easier caculation, we don't care hours.
			var fiveMin = 60 * 5; //five minutes is 300 seconds!
			var timeleft = fiveMin - seconds % fiveMin; // let's say now is 01:30, then current seconds is 60+30 = 90. And 90%300 = 90, finally 300-90 = 210. That's the time left!
			var result = parseInt(timeleft / 60) + ':' + timeleft % 60; //formart seconds back into mm:ss 
			document.getElementById('demo').innerHTML = "   (Automatic data updates in : "+result+" minutes)";
			if (result == "0:1") {
				window.location.href = "./api/api-redata.php?page=incidents";
			}
		}, 1000);
	</script>

</body>

</html>