<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<link rel="canonical" href="https://demo-basic.adminkit.io/login.php" />
	<title>Login</title>
	<link href="css/app.css" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
</head>
<style>
	.appBackground {
		position: relative;
		/* background-image: url("./img/bg/bg16.jpg"); */
		background: rgb(2, 0, 36);
		background: linear-gradient(90deg, rgba(2, 0, 36, 1) 0%, rgba(9, 121, 103, 1) 100%, rgba(0, 212, 255, 1) 100%);
		background-repeat: no-repeat;
		background-size: 100% 100vh;
	}

	body {
		font-family: 'Ubuntu', sans-serif !important;
		font-size: 20px;
	}
</style>

<body>
	<?php include "loader.php"; ?>
	<main class="d-flex w-100 appBackground">
		<div class="container d-flex flex-column" style="opacity:0.85;">
			<div class="row vh-100">
				<div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
					<div class="d-table-cell align-middle">
						<div class="card">
							<div class="card-body">
								<div class="m-sm-4">
									<div class="text-center">
										<center>
											<br />
											<h1 style="font-family: 'Bebas Neue'; font-size:70px; ">MDR Center</h1>
											<br />
										</center>
									</div>
									<form class="log-in" method="post" action="./api/api-login.php">
										<div class="mb-3">
											<label class="form-label">Username</label>
											<input class="form-control form-control-lg" name="username" type="text" autofocus placeholder="Username" />
										</div>
										<div class="mb-3">
											<label class="form-label">Password</label> &nbsp;
											<input class="form-control form-control-lg" id="password" name="password" type="password" placeholder="Password" />
											<br />
											<small>
												<a href="#">Forgot password?</a>
											</small>
										</div>
										<div class="text-center mt-3">
											<button type="submit" onclick="showLoader()" class="btn btn-lg btn-dark" style=" color:aliceblue;">Sign in</button>
											<!-- <button type="submit" class="btn btn-lg btn-primary">Sign in</button> -->
										</div>

										<div class="text-center mt-4">
											<br>
											<!-- <p>Powered by </br><img src="img/avatars/logo_pro.png" style="margin-top:10px;" alt="Charles Hall" class="img-fluid " width="150" height="150" /></p> -->
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<script src="js/app.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script>
		function submitResult(e) {
			e.preventDefault();
			let timerInterval
			Swal.fire({
				title: 'กำลังเข้าสู่ระบบ',
				html: '',
				timerProgressBar: true,
				didOpen: () => {
					Swal.showLoading()
					const b = Swal.getHtmlContainer().querySelector('b')
					timerInterval = setInterval(() => {
						b.textContent = Swal.getTimerLeft()
					}, 100)
				},
				willClose: () => {
					clearInterval(timerInterval)
				}
			}).then((result) => {
				/* Read more about handling dismissals below */
				if (result.dismiss === Swal.DismissReason.timer) {
					console.log('I was closed by the timer')
				}
			})
		}
	</script>
</body>

</html>