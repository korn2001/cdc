<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
	<link rel="canonical" href="https://demo-basic.adminkit.io/" />
	<title>MDR Center</title>
	<link href="css/app.css" rel="stylesheet">
</head>

<body>
	<div class="wrapper">
		<?php include 'sidebar.php'; ?>
		<?php include './api/graph.php'; ?>
		<div class="main">
			<?php include 'navbar.php'; ?>
			<main class="content" style="padding :15px;">
				<div class="container-fluid p-0">
					<div class="row">
						<h1 class="h3 mb-3"><strong>Threat Intelligence News</strong></h1>
						<div class="row">
							<div class="col-12 col-lg-12 col-xxl-12 d-flex">
								<div class=" flex-fill" style="overflow-x:auto;">
									<?php include './feed_all.php'; ?>
								</div>
							</div>
						</div>
					</div>
			</main>
			<?php include './footer.php'; ?>
		</div>
	</div>
	<script src="js/app.js"></script>
</body>

</html>