<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<?php include 'navbar.php' ?>
	<br />
	<div class="container">
		<div class="row">
			<h3>NEWS MANAGE</h3>
			<br />
			<form action="../api/createnews.php" method="post" enctype="multipart/form-data">
				<table class="table mt-5">
					<thead>
						<tr>
							<th scope="col">Name</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<input class="form-control" name="name" type="text" placeholder="name">
							</td>
						</tr>
					</tbody>
				</table>


				<table class="table mt-3">
					<thead>
						<tr>
							<th scope="col">Detail News</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<label for="w3review">Paragraph 1</label>
								<textarea class="form-control" name="detail1" rows="4" cols="50"></textarea>

								<label for="w3review">Paragraph 2</label>
								<textarea class="form-control" name="detail2" rows="4" cols="50"></textarea>

								<label for="w3review">Paragraph 3</label>
								<textarea class="form-control" name="detail3" rows="4" cols="50"></textarea>
						</tr>
					</tbody>
				</table>

				<table class="table mt-3">
					<thead>
						<tr>
							<th scope="col">User</th>
							<th scope="col">Date</th>
							<th scope="col">Reference</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<input class="form-control" name="user" type="text" placeholder="user">
							</td>
							<td>
								<input class="form-control" name="date" type="text" placeholder="date">
							</td>
							<td>
								<input class="form-control" name="reference" type="text" placeholder="Reference">
							</td>
						</tr>
					</tbody>
				</table>


				<table class="table mt-3">
					<thead>
						<tr>
							<th scope="col">Upload Image</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>

								Select image to upload:
								<input type="file" name="fileToUpload" id="fileToUpload">
								<input type="submit" value="Add News" class="btn btn-success" name="submit">

							</td>
						</tr>
					</tbody>
				</table>
			</form>

			<table class="table mt-3">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">Detail1</th>
						<!-- <th scope="col">Detail2</th>
						<th scope="col">Detail3</th> -->
						<th scope="col">User</th>
						<th scope="col">Date</th>
						<th scope="col">Reference</th>
						<!-- <th scope="col">Edit</th> -->
						<th scope="col">Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php
					include("../api/config.php");
					$sql = "SELECT * FROM tbl_news";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) {
					?>
							<tr>
								<td><?php echo $row["id"] ?></td>
								<td><?php echo $row["name"] ?></td>
								<td><?php echo $row["detail1"] ?></td>
								<!-- <td><?php echo $row["detail2"] ?></td>
								<td><?php echo $row["detail3"] ?></td> -->
								<td><?php echo $row["user"] ?></td>
								<td><?php echo $row["date"] ?></td>
								<td><?php echo $row["link"] ?></td>
								<td><a href="../api/deletenews.php?id=<?php echo $row["id"] ?>"><button type="button" class="btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-minus-fill" viewBox="0 0 16 16">
												<path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM6 8.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1 0-1z" />
											</svg></button></a></td>
							</tr>
					<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>

	<?php require '../api/close.php'; ?>

	<!-- Optional JavaScript -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>