<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
	<?php include 'navbar.php' ?>
	<br />

	<div class="container">
		<div class="row">
			<h3>USER MANAGE</h3>

			<table class="table mt-3">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">Username</th>
						<th scope="col">Role</th>
						<th scope="col">Company</th>
						<th scope="col">Add</th>

					</tr>
				</thead>
				<tbody>
					<tr>
						<form action="../api/createuser.php" method="POST">
							<td></td>
							<td>
								<input class="form-control" name="fullname" type="text" placeholder="fullname">
							</td>
							<td>
								<input class="form-control" name="username" type="text" placeholder="username">
							</td>
							<td>
								<select class="form-control" id="role" name="role" placeholder="role">
									<option value="user">User</option>
									<option value="admin">Admin</option>
								</select>

							</td>
							<td>
								<select class="form-control" id="company" name="company" placeholder="company">
									<option value="" disabled selected>Select Company</option>
									<?php
									include("../api/config.php");
									$sql = "SELECT * FROM tbl_company";
									$result = $conn->query($sql);
									if ($result->num_rows > 0) {
										while ($row = $result->fetch_assoc()) {
									?>
											<option value="<?php echo $row["id_company"] ?>"><?php echo $row["name_company"] ?></option>
									<?php
										}
									}
									?>
								</select>

							</td>
							<td>
								<button type="submit" class="btn btn-success">Add</button>
							</td>
						</form>
					</tr>

				</tbody>
			</table>
			<table class="table mt-3">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">Username</th>
						<th scope="col">Password</th>
						<th scope="col">Company</th>
						<th scope="col">Role</th>
						<!-- <th scope="col">Edit</th> -->
						<th scope="col">Delete</th>
					</tr>
				</thead>
				<tbody>
					<!-- <button style="width: 100%;" type="button" class="btn btn-info" data-toggle="modal" data-target=".bd-example-modal-lg">Add User</button> -->
					<?php
					include("../api/config.php");
					$sql = "SELECT * FROM tbl_user INNER JOIN tbl_company ON tbl_user.company=tbl_company.id_company";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_assoc()) {
					?>
							<tr>
								<td><?php echo $row["id"] ?></td>
								<td><?php echo $row["fullname"] ?></td>
								<td><?php echo $row["username"] ?></td>
								<td><?php echo "******" . substr($row["password"], strlen($row["password"]) - 3); ?></td>
								<td><?php echo $row["name_company"] ?></td>
								<td><?php echo $row["role"] ?></td>
								<!-- <td><button type="button" class="btn btn-warning"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
											<path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
										</svg></button></td> -->
								<td><a href="../api/deleteuser.php?id=<?php echo $row["id"] ?>"><button type="button" class="btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-minus-fill" viewBox="0 0 16 16">
												<path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM6 8.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1 0-1z" />
											</svg></button></a></td>
							</tr>
					<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>

	<?php require '../api/close.php'; ?>

	<!-- Modal -->


	<!-- Modal -->
	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5>Add Event</h5>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="../api/createevent.php" method="POST">
						<input class="form-control" name="date" type="date" style="width:100%;" placeholder="date">
						<br />
						<input class="form-control" name="name" type="text" style="width:100%;" placeholder="name">
						<br />
						<input class="form-control" name="user" type="text" style="width:100%;" placeholder="user">
						<br />
						<input class="form-control" name="type" type="text" style="width:100%;" placeholder="type" value="event" readonly>
						<br />
						<input class="form-control" name="ref" type="text" style="width:100%;" placeholder="ref">
						<br />
						<textarea class="form-control" name="detail1" cols="80" rows="6" placeholder="detail1"></textarea>
						<br />
						<textarea class="form-control" name="detail2" cols="80" rows="6" placeholder="detail2"></textarea>
						<br />
						<textarea class="form-control" name="detail3" cols="80" rows="6" placeholder="detail3"></textarea>
						<br />
						<br />
						<button type="submit" class="btn btn-success">Add Event</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</form>
				</div>
				<!-- <div class="modal-footer">
					
				</div> -->
			</div>
		</div>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>