<?php
session_start();
if (isset($_SESSION['username'])) {
} else {
	echo '<script>  document.location.href = "./error_login.php";</script> ';
	exit;
}
?>
<?php
    if($_SESSION["lang"] == "EN"){
        include("./language/en.php");
    }
    else{
        include("./language/th.php");
    }
?>

<style>
	@media screen and (max-width: 425px) {
		.hidden_mobile {
			visibility: hidden;
			clear: both;
			float: left;
			margin: 10px auto 5px 20px;
			width: 28%;
			display: none;
		}
	}
	body {
		font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif;
	}
</style>

<nav id="sidebar" class="sidebar js-sidebar">
	<div class="sidebar-content js-simplebar">
		<!-- <a class="sidebar-brand" href="#"> -->
		<!-- <span class="align-middle"><?php echo $_SESSION["company"]; ?></span> -->
		<!-- </a> -->
		<h2 class="sidebar-brand" style="padding-bottom: 10px;">
			<span class="align-middle" style="font-size: 20px;">MDR Center</span>
		</h2>
		<ul class="sidebar-nav">
			<!-- ======================================== -->
			<?php if ($_SESSION["role"] == "partner") { ?>
				<li class="sidebar-header">
					<?php echo $strManagement;?>
				</li>
				<li class="sidebar-item">
					<a class="sidebar-link" href="dashboard.php">
						<i class="align-middle" data-feather="compass"></i> <span style="font-size: 14px;" class="align-middle"><?php echo $strTenant;?></span>
					</a>
				</li>
			<?php } ?>
			<!-- ======================================== -->
			<li class="sidebar-header">
				<?php echo $strHome;?>
			</li>
			<li class="sidebar-item">
				<a class="sidebar-link" href="home.php">
					<i class="align-middle" data-feather="pie-chart"></i> <span style="font-size: 14px;" class="align-middle"><?php echo $strDashboard;?></span>
				</a>
			</li>

			<li class="sidebar-item">
				<a class="sidebar-link" href="endpoint.php">
					<i class="align-middle" data-feather="monitor"></i> <span style="font-size: 14px;" class="align-middle"><?php echo $strEndpoint;?></span>
				</a>
			</li>
			<!-- ======================================== -->
			<li class="sidebar-header">
			<?php echo $strIncidentEvent;?>
			</li>

			<li class="sidebar-item">
				<a class="sidebar-link" href="incidents.php">
					<i class="align-middle" data-feather="airplay"></i> <span style="font-size: 14px;" class="align-middle"><?php echo $strIncidents;?></span>
				</a>
			</li>

			<?php if ($_SESSION["hunting"]) {
				echo '<li class="sidebar-item">
						<a class="sidebar-link" href="incidents-hunting.php">
							<i class="align-middle" data-feather="minus-circle"></i> <span style="font-size: 14px;" class="align-middle">Hunting</span>
						</a>
					</li>';
			} ?>


			<!-- ======================================== -->

			<?php if ($_SESSION["ticket"]) {
				echo '
			<li class="sidebar-header">
				Ticket
			</li>

			<li class="sidebar-item">
				<a class="sidebar-link" href="incidents-ticket.php">
					<i class="align-middle" data-feather="activity"></i> <span style="font-size: 14px;" class="align-middle">Ticket Summary</span>
				</a>
			</li>';
			} ?>

			<!-- ======================================== -->
			<li class="sidebar-header">
				<?php echo $strNews;?>
			</li>

			<li class="sidebar-item hidden_mobile">
				<a class="sidebar-link" href="news.php">
					<i class="align-middle" data-feather="book"></i> <span style="font-size: 14px;" class="align-middle"><?php echo $strThreatIntelligenceNews;?></span>
				</a>
			</li>

			<li class="sidebar-item">
				<a class="sidebar-link" href="cyber-news.php">
					<i class="align-middle" data-feather="paperclip"></i> <span style="font-size: 14px;" class="align-middle"><?php echo $strCyberSecurityNews;?></span>
				</a>
			</li>
			<li class="sidebar-header">
				<strong>Language</strong>
				<a href="change.php?lang=TH">TH</a> , <a href="change.php?lang=EN">EN</a>
			</li>
		</ul>
	</div>
</nav>
